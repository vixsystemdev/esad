<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Página não encontrada</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="shortcut icon" type="image/png" href="<?php echo HOME_URI;?>/views/favicon_essencial.png">

	<link rel="stylesheet" href="<?php echo HOME_URI;?>/views/publico/assets/plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo HOME_URI;?>/views/publico/assets/css/style.css">

	<!-- CSS Header and Footer -->
	<link rel="stylesheet" href="<?php echo HOME_URI;?>/views/publico/assets/css/headers/header-default.css">
	<link rel="stylesheet" href="<?php echo HOME_URI;?>/views/publico/assets/css/footers/footer-v1.css">

	<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="<?php echo HOME_URI;?>/views/publico/assets/plugins/animate.css">
	<link rel="stylesheet" href="<?php echo HOME_URI;?>/views/publico/assets/plugins/line-icons/line-icons.css">
	<link rel="stylesheet" href="<?php echo HOME_URI;?>/views/publico/assets/plugins/font-awesome/css/font-awesome.min.css">

	<!-- CSS Page Style -->
	<link rel="stylesheet" href="<?php echo HOME_URI;?>/views/publico/assets/css/pages/page_404_error.css">

	<!-- CSS Theme -->
	<link rel="stylesheet" href="<?php echo HOME_URI;?>/views/publico/assets/css/theme-colors/blue.css" id="style_color">
	<link rel="stylesheet" href="<?php echo HOME_URI;?>/views/publico/assets/css/theme-skins/dark.css">

	<!-- CSS Customization -->
	<link rel="stylesheet" href="<?php echo HOME_URI;?>/views/publico/assets/css/custom.css">

</head>
<style>
body { 
    background-image: url('<?php echo HOME_URI;?>/views/back.png');
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-position: center; 
}
</style>
<body>
		<!--=== Content Part ===-->
		<div class="container content" style="margin-top:20px;">
			<!--Error Block-->
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="error-v1">
						<span class="error-v1-title">404</span>
						<span>Página não encontrada!</span>
						<p>Desculpe, mas a página que você está procurando não foi encontrada. <br>
    Verifique se digitou o endereço da URL corretamente.</p>
						<a class="btn-u btn-bordered" href="<?php echo HOME_URI;?>/">Voltar para Página Inicial</a>
					</div>
				</div>
			</div>
			<!--End Error Block-->
		</div>
		<!--=== End Content Part ===-->
</body>
</html>
