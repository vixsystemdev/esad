<nav id="navbar" class="navbar navbar-toggleable-sm navbar-light bg-faded fixed-top">
  <button class="navbar-toggler navbar-toggler-left collapsed" type="button" data-toggle="collapse" data-target="#navbarMenu" aria-controls="navbarMenu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="<?php echo HOME_URI?>">
    <img class="brand default" src="<?php echo HOME_URI?>/views/public/assets/esad/img/logo-esad.png" alt="esad">
  </a>
  <div class="nav-gradient">
  </div>
  
  <div class="collapse navbar-collapse justify-content-end" id="navbarMenu">
    <button class="navbar-toggler navbar-toggler-left close" type="button" data-toggle="collapse" data-target="#navbarMenu" aria-controls="navbarMenu" aria-expanded="false" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    <ul class="navbar-nav">
      <li class="nav-item <?php if($page == "meuescritorio") { echo "active"; } ?>">
        <a href="<?php echo HOME_URI?>/plataforma/meuescritorio/" class="nav-link">
          meu escritório
        </a>
        <div class="nav-gradient-inner">
          <div class="container">
            <ul class="nav">
              <li class="nav-item">
                <a class="nav-link" href="<?php echo HOME_URI?>/plataforma/processos/">Meus Processos</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo HOME_URI?>/plataforma/andamentos/">Andamentos</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo HOME_URI?>/plataforma/atividades/">Atividades</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo HOME_URI?>/plataforma/arquivos/">Arquivos</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo HOME_URI?>/plataforma/contatos/">Contatos</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo HOME_URI?>/plataforma/clientes/">Clientes</a>
              </li>
            </ul>
          </div>
        </div>
      </li>
      <li class="nav-item <?php if($page == "informativos") { echo "active"; } ?>">
        <a href="<?php echo HOME_URI?>/plataforma/informativos/" class="nav-link">
          informativos
        </a>
      </li>
      <li class="nav-item <?php if($page == "biblioteca") { echo "active"; } ?>">
        <a href="<?php echo HOME_URI?>/plataforma/biblioteca/" class="nav-link">biblioteca</a>
      </li>
      <li class="nav-profile dropdown">
        <button type="button" class="nav-link dropdown-toggle" id="profileMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <img src="<?php echo HOME_URI?>/views/public/assets/esad/temp/profile-leonardo.jpg" alt="Perfil">
        </button>
        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="profileMenu">
          <li class="dropdown-item" >
            <a href="<?php echo HOME_URI?>/plataforma/perfil/">
            	Meu Perfil
            </a>
          </li>
          <li class="dropdown-item" >
            <a href="<?php echo HOME_URI?>/plataforma/preferencias/">
              Preferências
            </a>
          </li>
          <li class="dropdown-item" >
            <a href="<?php echo HOME_URI?>/plataforma/plano/">
              Meu Plano
            </a>
          </li>
          <div class="dropdown-divider">
          </div>
          <li class="dropdown-item" >
            <a href="<?php echo HOME_URI?>/logout/">
              Sair
            </a>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</nav>