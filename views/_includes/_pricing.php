<div class="pricing">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="card card-collapse card-01">
          <div class="card-header">
            <h2>
              Básico
            </h2>
          </div>
          <div class="card-block">
            <div class="card-title">
              <h3>
                <br class="hidden-sm-down">
                Gratuito
              </h3>
            </div>
            <ul class="card-text">
              <li>
                Gerencie até 3 processos
              </li>
              <li>
                Profissionalize seus contatos
              </li>
              <li>
                Organize suas atividades
              </li>
              <li class="no-available">
                Relacione-se com seus clientes
              </li>
              <li class="no-available">
                Acompanhe informativos
              </li>
              <li class="no-available">
                Armazene arquivos processuais
              </li>
              <li>
                Compartilhe informações
              </li>
            </ul>
            <a href="<?php echo HOME_URI?>/plataforma/" class="btn btn-info btn-lg btn-block">
              <?php
                if(($plano == NULL) || ($plano != "Básico")) {
                  echo "Começar";
                } else {
                  echo "Seu Plano Atual";
                }
              ?>
            </a>
          </div>
          <div class="card-footer">
            Este plano é indicado para advogados que iniciaram sua carreira recentemente ou para aqueles que querem conhecer o esad.
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card card-collapse card-02">
          <div class="card-header">
            <h2>
              Premium
            </h2>
          </div>
          <div class="card-block">
            <div class="card-title">
              <p class="from">
                À partir de:
              </p>
              <h3>
                R$ 9,50 <small class="currency">/mês</small>
              </h3>
              <p class="details">
                + R$ 2,50 por processo ativo cadastrado.
              </p>
            </div>
            <ul class="card-text">
              <li>
                Gerencie todos seus processos
              </li>
              <li>
                Profissionalize seus contatos
              </li>
              <li>
                Organize suas atividades
              </li>
              <li>
                Relacione-se com seus clientes
              </li>
              <li>
                Acompanhe informativos
              </li>
              <li>
                Armazene arquivos processuais
              </li>
              <li>
                Compartilhe informações
              </li>
            </ul>
            <a href="<?php echo HOME_URI?>/plataforma/" class="btn btn-info btn-lg btn-block">
              <?php
                if(($plano == NULL) || ($plano != "Premium")) {
                  echo "Começar";
                } else {
                  echo "Seu Plano Atual";
                }
              ?>
            </a>
          </div>
          <div class="card-footer">
            Este plano é indicado para advogados que tem uma certa quantidade de processos a serem gerenciados (entre 4 e 25 processos) ou que querem utilizar todas as funcionalidades oferecidas pelo esad.
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card card-collapse card-03">
          <div class="card-header">
            <h2>
              Ilimitado
            </h2>
          </div>
          <div class="card-block">
            <div class="card-title">
              <h3>
                R$ 59,50 <small class="currency">/mês</small>
              </h3>
              <p class="details">
                Cadastre quantos processos quiser, desfrutando de todos os recursos oferecidos.
              </p>
            </div>
            <ul class="card-text">
              <li>
                Gerencie todos seus processos
              </li>
              <li>
                Profissionalize seus contatos
              </li>
              <li>
                Organize suas atividades
              </li>
              <li>
                Relacione-se com seus clientes
              </li>
              <li>
                Acompanhe informativos
              </li>
              <li>
                Armazene arquivos processuais
              </li>
              <li>
                Compartilhe informações
              </li>
            </ul>
            <a href="<?php echo HOME_URI?>/plataforma/" class="btn btn-info btn-lg btn-block">
              <?php
                if(($plano == NULL) || ($plano != "Ilimitado")) {
                  echo "Começar";
                } else {
                  echo "Seu Plano Atual";
                }
              ?>
            </a>
          </div>
          <div class="card-footer">
            Este plano é indicado para advogados que já tem uma grande quantidade de processos (mais de 25 processos), aproveitando ao máximo as funcionalidades organizacionais do esad.
          </div>
        </div>
      </div>
    </div>
  </div>
</div>