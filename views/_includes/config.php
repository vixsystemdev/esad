<?php

  // Define a pasta de escopo do projeto
  function reverse_strrchr($haystack, $needle) {
    $pos = strrpos($haystack, $needle);
	
	

    if($pos === false) {
      return $haystack;
    }

    return substr($haystack, 0, $pos + 0);
  }

  $path_public  = $_SERVER['DOCUMENT_ROOT'];
  $path_root    = reverse_strrchr($path_public, '/');


  // Define a pasta padrão dos includes
  set_include_path($path_root.PATH_SEPARATOR.$path_public);

?>