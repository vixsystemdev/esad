<ul class="nav flex-column nav-pills">
	<li class="nav-item text-left">
    
    <a class="nav-link" href="<?php echo HOME_URI?>/plataforma/arquivos/materia/">
    	<i class="fa fa-folder" aria-hidden="true"></i>
    	Administrativo
    </a>
	</li>
	<li class="nav-item text-left">
		<a class="nav-link" href="<?php echo HOME_URI?>/plataforma/arquivos/materia/">
			<i class="fa fa-folder" aria-hidden="true"></i>
			Civel
		</a>
	</li>
	<li class="nav-item text-left">
		<a class="nav-link" href="<?php echo HOME_URI?>/plataforma/arquivos/materia/">
			<i class="fa fa-folder" aria-hidden="true"></i>
			Do Trabalho
		</a>
	</li>
	<li class="nav-item text-left">
		<a class="nav-link" href="<?php echo HOME_URI?>/plataforma/arquivos/materia/">
			<span class="fa-stack fa-lg share">
	    	<i class="fa fa-folder fa-stack-2x" aria-hidden="true"></i>
	    	<i class="fa fa-user fa-inverse fa-stack-1x pull-right" aria-hidden="true"></i>
    	</span>
			Família
		</a>
	</li>
	<li class="nav-item text-left">
		<a class="nav-link" href="<?php echo HOME_URI?>/plataforma/arquivos/materia/">
			<i class="fa fa-folder" aria-hidden="true"></i>
			Penal
		</a>
	</li>
	<li class="nav-item text-left">
		<a class="nav-link" href="<?php echo HOME_URI?>/plataforma/arquivos/materia/">
			<span class="fa-stack fa-lg share">
	    	<i class="fa fa-folder fa-stack-2x" aria-hidden="true"></i>
	    	<i class="fa fa-user fa-inverse fa-stack-1x pull-right" aria-hidden="true"></i>
    	</span>
			Tributário
		</a>
	</li>
	<li class="nav-item text-left">
		<a class="nav-link" href="<?php echo HOME_URI?>/plataforma/arquivos/materia/">
			<span class="fa-stack fa-lg share">
	    	<i class="fa fa-folder fa-stack-2x" aria-hidden="true"></i>
	    	<i class="fa fa-user fa-inverse fa-stack-1x pull-right" aria-hidden="true"></i>
    	</span>
			Outros
		</a>
	</li>
</ul>