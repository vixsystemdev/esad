<nav id="navbar" class="navbar navbar-toggleable-sm navbar-light bg-faded fixed-top">
  <button class="navbar-toggler navbar-toggler-left collapsed" type="button" data-toggle="collapse" data-target="#navbarMenu" aria-controls="navbarMenu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon77"></span>
  </button>
  <a class="navbar-brand" href="<?php echo HOME_URI?>/admin/">
    <img class="brand default" src="/assets/esad/img/logo-esad.png" alt="esad">
  </a>
  <div class="nav-gradient">
  </div>
  <div class="collapse navbar-collapse justify-content-end" id="navbarMenu">
    <button class="navbar-toggler navbar-toggler-left close" type="button" data-toggle="collapse" data-target="#navbarMenu" aria-controls="navbarMenu" aria-expanded="false" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    <ul class="navbar-nav">
      <li class="nav-item <?php if($page == "admin-usuarios") { echo "active"; } ?>">
        <a href="<?php echo HOME_URI?>/admin/usuarios/" class="nav-link">
          usuários
        </a>
      </li>
      <li class="nav-item <?php if($page == "admin-processos") { echo "active"; } ?>">
        <a href="<?php echo HOME_URI?>/admin/processos/" class="nav-link">
          processos
        </a>
      </li>
      <li class="nav-item <?php if($page == "admin-config") { echo "active"; } ?>">
        <a href="#" class="nav-link">
          configurações
        </a>
      </li>
      <li class="nav-profile dropdown">
        <button type="button" class="nav-link dropdown-toggle" id="profileMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <img src="/assets/esad/temp/profile-leonardo.jpg" alt="Perfil">
        </button>
        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="profileMenu">
          <li class="dropdown-item" >
            <a href="/admin/perfil/">
              Meu Perfil
            </a>
          </li>
          <li class="dropdown-item" >
            <a href="/admin/preferencias/">
              Preferências
            </a>
          </li>
          <div class="dropdown-divider">
          </div>
          <li class="dropdown-item" >
            <button type="button">
              Sair
            </button>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</nav>