<?php 
 $modelo_contatos->selecionar_contato(); 
 $modelo_contatos->insere_contato();
 $modelo_contatos->insere_tipo_contato();
 $modelo_contatos->apaga_contato();
?>



<div class="modal fade" id="cadastro-contato" tabindex="-1" role="dialog" aria-labelledby="uploadLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="uploadLabel">
          Informações do Contatos
        </h5>
      </div>
      <div class="modal-body">
        <p class="mb-4">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.
        </p>
        
        <form id="cadastrocontato" role="form" method="post" enctype="multipart/form-data" autocomplete="off">
         <input type="hidden" class="form-control" name="tipocliente"  value="1">
         <input type="hidden" class="form-control" name="status"  value="1">
         <input type="hidden" class="form-control" name="insere_contato"  value="1">
          <div class="form-group row">
            <label class="col-sm-4 col-form-label" for="nome">Nome completo</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="nome" name="nome" placeholder="" value="<?php echo chk_array($modelo_contatos->form_data, 'nome'); ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-4 col-form-label" for="empresa">Empresa</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="empresa" name="empresa" placeholder="" value="<?php echo chk_array($modelo_contatos->form_data, 'empresa'); ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-4 col-form-label" for="celular">Celular</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="celular" data-mask="(00) 9 0000-0000" name="celular" placeholder="(99) 9 9999 9999" value="<?php echo chk_array($modelo_contatos->form_data, 'celular'); ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-4 col-form-label" for="telefone">Telefone</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="telefone" maxlength="15" name="telefone" placeholder="" value="<?php echo chk_array($modelo_contatos->form_data, 'telefone'); ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-4 col-form-label" for="email">Email</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="email" name="email" placeholder="" value="<?php echo chk_array($modelo_contatos->form_data, 'email'); ?>">
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <a href="<?php echo HOME_URI . '/plataforma/contatos' ?>" class="btn btn-default">Cancelar</a>
        <button form="cadastrocontato" type="submit" class="btn btn-success">Avançar</a>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="excluir-contato" tabindex="-1" role="dialog" aria-labelledby="uploadLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="uploadLabel">
          Excluir Contatos
        </h5>
      </div>
      <div class="modal-body">
        <p class="mb-4">
          Tem certeza que deseja excluir este contato?
        </p>        
       	
      </div>
      <div class="modal-footer">
        <a href="<?php echo HOME_URI . '/plataforma/contatos' ?>" class="btn btn-default">Não</a>
        <a href="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '/confirma'; ?> " class="btn btn-default">Sim, tenho certeza!</a>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="tipo-contato" tabindex="-1" role="dialog" aria-labelledby="uploadLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="uploadLabel">
          Tipo do Contato
        </h5>
      </div>
      <div class="modal-body">
        <p class="mb-4">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.
        </p>
        <form action="">
        
        
          <div class="form-group d-inline-block">
            <div class="checkbox form-control square">
              <label for="">
                <input type="checkbox" id="cbx[]"  name="cbx[]" value="cbx1">
                <span>
                  Advogado
                </span>
              </label>
            </div>
            <div class="checkbox form-control square">
              <label for="">
                <input type="checkbox" id="cbx[]" name="cbx[]" value="cbx2">
                <span>
                  Lorem Ipsum
                </span>
              </label>
            </div>
            
            <div class="checkbox form-control square">
              <label for="">
                <input type="checkbox" id="cbx[]" name="cbx[]" value="cbx3">
                <span>
                  Ciente
                </span>
              </label>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
      	<a href="<?php echo HOME_URI . '/plataforma/contatos' ?>" class="btn btn-default">Cancelar</a>
        <button type="button" class="btn btn-success">Avançar</a>
      </div>
    </div>
  </div>
</div>

 
<div class="modal fade" id="processos-contato" tabindex="-1" role="dialog" aria-labelledby="uploadLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="uploadLabel">
          Processos Associados
        </h5>
      </div>
      <div class="modal-body">
        <p class="mb-4">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.
        </p>
        <form action="">
          <div class="form-group">
            <div class="input-group">
              <input name="processo" id="processo" type="text" class="form-control" placeholder="Localizar processo">
              <span class="input-group-btn">
                <button class="btn btn-secondary" type="button"  onclick="buscarprocesso()" >
                  <i class="fa fa-plus" aria-hidden="true"></i>
                </button>
              </span>
            </div>
          </div>
          <hr>
          
        
          <?php $lista_processo = $modelo_contatos->selecionar_processos_cliente();
				foreach($lista_processo as $processos):?>
                
                 <div class="form-group">
                    <div class="input-group">
                      <input name="processo" id="processo" type="text" class="form-control" value="<?php echo $processos['num_processo']?>" disabled>
                      <span class="input-group-btn">
                       	 <a href="<?php echo HOME_URI; ?>/plataforma/contatos/delpro/<?php echo $processos['id']?>" class="btn btn-secondary"> <i class="fa fa-trash-o"></i> </a>
                        <button data-toggle="modal" id="delprocesso" data-target="#excluir-processo" type="button" class="text-danger" data-placement="top" title="Excluir" style="display:none;">
                          <i class="fa fa-trash-o"></i>
                        </button>
                      </span>
                    </div>
                  </div>
                                 
           <?php endforeach ?>
         
         
        </form>
      </div>
      <div class="modal-footer">
        <a href="<?php echo HOME_URI . '/plataforma/contatos/' ?>" class="btn btn-default">Cancelar</a>
        <button data-dismiss="modal" type="button" class="btn btn-success">Concluir</a>
      </div>
    </div>
  </div>
</div>

<!--Modal de feedback-->

<div class="modal fade" id="enviar-feedback" tabindex="-1" role="dialog" aria-labelledby="uploadLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="uploadLabel">
              Selecionar Processo
            </h5>
          </div>
          <div class="modal-body">
            <p class="mb-4">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.
            </p>
            <form action="">
              <div class="form-group row">
                <label class="col-sm-4 col-form-label" for="lorem">Cliente</label>
                <div class="col-sm-8">
                  <select class="form-control custom-select" id="lorem">
                    <option><?php echo chk_array($modelo_contatos->form_data, 'nome'); ?></option>
                  </select>
                </div>
              </div>
               <div class="form-group row">
                <label class="col-sm-4 col-form-label" for="ipsum">Matéria</label>
                <div class="col-sm-8">
                  <select class="form-control custom-select" id="ipsum">
					   <?php $lista_atividade = $modelo_contatos->selecionar_processos_atividades();
                            foreach($lista_atividade as $atividades):?>
                            <option><?php echo $atividades['atividade']?></option>
                       <?php endforeach ?>
					 
                   
                  </select>
                </div>
              </div>
               <div class="form-group row">
                <label class="col-sm-4 col-form-label" for="darnet">Nº do processo:</label>
                <div class="col-sm-8">
                  <select class="form-control custom-select" id="darnet">
                  		<?php $lista_processo = $modelo_contatos->selecionar_processos();
                            foreach($lista_processo as $processos):?>
                            <option><?php echo $processos['num_processo']?></option>
                       <?php endforeach ?>
                    
                  </select>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
          	<a href="<?php echo HOME_URI . '/plataforma/contatos/' ?>" class="btn btn-default">Cancelar</a>
            <a href="<?php echo HOME_URI?>/plataforma/feedback/id/<?php echo chk_array($modelo_contatos->form_data, 'id'); ?>" class="btn btn-success">Avançar</a>
          </div>
        </div>
      </div>
    </div>
    
    
    
<!-- Excluir processos -->
    
<div class="modal fade" id="excluir-processo" tabindex="-1" role="dialog" aria-labelledby="uploadLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="uploadLabel">
          Excluir Cliente
        </h5>
      </div>
      <div class="modal-body">
        <p class="mb-4">
          Tem certeza que deseja excluir este processo?
        </p>        
       	
      </div>
      <div class="modal-footer">
        <a href="<?php echo HOME_URI . '/plataforma/contatos' ?>" class="btn btn-default">Não</a>
        <a href="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '/confirma'; ?> " class="btn btn-default">Sim, tenho certeza!</a>
      </div>
    </div>
  </div>
</div>

<!-- Script  -->
<script>

	var k = window.location.toString().indexOf("delpro") > 0;
		if (k == true){
			$( "#delprocesso" ).click();
		}
    
	
	function buscarprocesso(){
		alert('oi');					
		busca = $("#processo").val();
		id = '<?php echo $processos['idcliente']?>';
		alert(id);		
		window.location.href = "<?php echo HOME_URI?>/plataforma/contatos/pro/"+id+"/buscar/" + busca;
						
	}
</script>



