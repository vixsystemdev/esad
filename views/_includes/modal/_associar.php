<div class="modal fade" id="associar-contato" tabindex="-1" role="dialog" aria-labelledby="associar-contato" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">
          Associar contato
        </h5>
      </div>
      <div class="modal-body">
        <p class="mb-4">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.
        </p>
        <form action="">
          <div class="form-group">
            <div class="input-group">
              <input name="contato" id="contato" type="text" class="form-control" placeholder="Localizar contato">
              <span class="input-group-btn">
                <button class="btn btn-secondary" type="button">
                  <i class="fa fa-search" aria-hidden="true"></i>
                </button>
              </span>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button data-dismiss="modal" data-toggle="modal" data-target="#cadastro-contato" type="button" class="btn btn-success">Avançar</a>
      </div>
    </div>
  </div>
</div>