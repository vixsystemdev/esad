<div class="modal fade" id="enviar-arquivos-select" tabindex="-1" role="dialog" aria-labelledby="enviar-arquivos-select" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">
          Enviar Arquivos
        </h5>
      </div>
      <div class="modal-body">
        <form action="">
          <div class="form-group row">
            <label class="col-sm-4 col-form-label" for="materia">Matéria</label>
            <div class="col-sm-8">
              <select class="form-control custom-select" id="materia">
                <option disabled="disabled">-</option>
                <option value="Administrativo">Administrativo</option>
                <option value="Cível">Cível</option>
                <option value="Trabalhista">Trabalhista</option>
                <option value="Família">Família</option>
                <option value="Penal">Penal</option>
                <option value="Tributário">Tributário</option>
                <option value="Outros">Outros</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-4 col-form-label" for="cliente">Cliente</label>
            <div class="col-sm-8">
              <select class="form-control custom-select" id="cliente">
                <option disabled="disabled">-</option>
                <option value="Felipe Lomeu">Felipe Lomeu</option>
                <option value="Matheus Machado">Matheus Machado</option>
                <option value="Nilson Rocha">Nilson Rocha</option>
                <option value="Carlos Guilherme">Carlos Guilherme</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-4 col-form-label" for="nProcesso">Nº do Processo:</label>
            <div class="col-sm-8">
              <select class="form-control custom-select" id="nProcesso">
                <option disabled="disabled">-</option>
                <option value="">0020508-82.2008.8.08.0035</option>
              </select>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-success" data-dismiss="modal" data-toggle="modal" data-target="#enviar-arquivos-upload">Avançar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="enviar-arquivos-upload" tabindex="-1" role="dialog" aria-labelledby="enviar-arquivos-upload" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">
          Enviar Arquivos
        </h5>
      </div>
      <div class="modal-body">
        <form action="">
          <label class="custom-file d-block">
            <input type="file" id="file" class="custom-file-input">
            <span class="custom-file-control">
              <span class="btn-download">
                <i class="fa fa-upload"></i>
                <span>Anexar Arquivos</span>
              </span>
              Você também pode arrastar arquivos para cá.
            </span>
          </label>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-success" data-dismiss="modal">Concluir</button>
      </div>
    </div>
  </div>
</div>