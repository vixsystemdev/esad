<?php $modelo_clientes->selecionar_cliente(); ?>

<div class="modal fade" id="cadastro-cliente" tabindex="-1" role="dialog" aria-labelledby="uploadLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="uploadLabel">
          Informações do Clientes
        </h5>
      </div>
      <div class="modal-body">
        <p class="mb-4">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.
        </p>
        <form action="">
          <div class="form-group row">
            <label class="col-sm-4 col-form-label" for="nome">Nome completo</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="nome" name="nome" placeholder="" value="<?php echo chk_array($modelo_clientes->form_data, 'nome'); ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-4 col-form-label" for="empresa">Empresa</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="empresa" name="empresa" placeholder="" value="<?php echo chk_array($modelo_clientes->form_data, 'empresa'); ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-4 col-form-label" for="celular">Celular</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="celular" name="celular" placeholder="" value="<?php echo chk_array($modelo_clientes->form_data, 'celular'); ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-4 col-form-label" for="telefone">Telefone</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="telefone" name="telefone" placeholder="" value="<?php echo chk_array($modelo_clientes->form_data, 'telefone'); ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-4 col-form-label" for="email">Email</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="email" name="email" placeholder="" value="<?php echo chk_array($modelo_clientes->form_data, 'email'); ?>">
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <a href="<?php echo HOME_URI . '/plataforma/clientes' ?>" class="btn btn-default">Cancelar</a>
        <button data-dismiss="modal" data-toggle="modal" data-target="#tipo-cliente" type="button" class="btn btn-success">Avançar</a>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="tipo-cliente" tabindex="-1" role="dialog" aria-labelledby="uploadLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="uploadLabel">
          Tipo do Cliente
        </h5>
      </div>
      <div class="modal-body">
        <p class="mb-4">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.
        </p>
        <form action="">
          <div class="form-group d-inline-block">
            <div class="checkbox form-control square">
              <label for="">
                <input type="checkbox" name="custom_checkbox square" value="1">
                <span>
                  Advogado
                </span>
              </label>
            </div>
            <div class="checkbox form-control square">
              <label for="">
                <input type="checkbox" name="custom_checkbox square" value="1">
                <span>
                  Lorem Ipsum
                </span>
              </label>
            </div>
            
            <div class="checkbox form-control square">
              <label for="">
                <input type="checkbox" name="custom_checkbox square" value="1">
                <span>
                  Ciente
                </span>
              </label>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button data-dismiss="modal" data-toggle="modal" data-target="#processos-cliente" type="button" class="btn btn-success">Avançar</a>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="processos-cliente" tabindex="-1" role="dialog" aria-labelledby="uploadLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="uploadLabel">
          Processos Associados
        </h5>
      </div>
      <div class="modal-body">
        <p class="mb-4">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.
        </p>
        <form action="">
          <div class="form-group">
            <div class="input-group">
              <input name="processo" id="processo" type="text" class="form-control" placeholder="Localizar processo">
              <span class="input-group-btn">
                <button class="btn btn-secondary" type="button">
                  <i class="fa fa-plus" aria-hidden="true"></i>
                </button>
              </span>
            </div>
          </div>
          <hr>
          
          <?php $lista = $modelo_clientes->selecionar_processos_cliente();
				foreach($lista as $clientes):?>
                <div class="form-group">
                	<div class="input-group">
                	   <input name="processo" id="processo" type="text" class="form-control" value="<?php echo $clientes['num_processo']?>" disabled>
                       <span class="input-group-btn">
                        <button class="btn btn-secondary" type="button">
                          <i class="fa fa-trash-o"></i>
                        </button>
                      </span>                    
                  	</div>
                </div>
                 
           <?php endforeach ?>
       
          
        </form>
      </div>
      <div class="modal-footer">
        <a href="<?php echo HOME_URI . '/plataforma/clientes' ?>" class="btn btn-default">Cancelar</a>
        <button data-dismiss="modal" type="button" class="btn btn-success">Concluir</a>
      </div>
    </div>
  </div>
</div>