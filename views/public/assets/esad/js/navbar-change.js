$(document).ready(function() {

  function navbar_change(){
    if($(this).scrollTop() < 75){
      $('#navbar').addClass("bg-transparent");
      $('#navbar').addClass("navbar-inverse");
      $('#navbar').removeClass("bg-faded");
      $('#navbar').removeClass("navbar-light");
    }

    else{
      $('#navbar').addClass("navbar-light");
      $('#navbar').addClass("bg-faded");
      $('#navbar').removeClass("navbar-inverse");
      $('#navbar').removeClass("bg-transparent");
    }
  };
  navbar_change();

  $(window).scroll(function() {
    navbar_change();
  });

});