$(document).ready(function() {

  $('.navbar-toggler').click(function(e){
    $('body').addClass("overflow");
    $('nav.navbar').addClass("navbar-collapsed");
  });

  $('.close').click(function(e){
    $('body').removeClass("overflow");
    $('nav.navbar').removeClass("navbar-collapsed");
  });

  $(window).resize(function() {
    $('body').removeClass("overflow");
    $('nav.navbar').removeClass("navbar-collapsed");
    $('.navbar-collapse').removeClass("show");
  });

});