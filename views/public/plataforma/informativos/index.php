<?php
	 // General settings (relative path only here)
	 require ABSPATH . '/views/_includes/config.php';

	 $page = "informativos";
?>

<!DOCTYPE html>
<html>
	<head>
		<?php require ABSPATH . '/views/_includes/metadata.php'; ?>
		<title>Informativos | esad</title>
		<?php require ABSPATH . '/views/_includes/styles.php'; ?>
	</head>
	<body>
		<div class="wrapper">
			<div class="layout-navbar layout-fluid <!--?php echo $template; ?-->">
				<header>
					<?php require ABSPATH . "/views/_includes/navbar-fluid-plataforma.php"; ?>
				</header>
				<div class="wrapper-main">
					<div class="wrapper-content">
						<div class="container">
              <section>
  							<div class="row ">
                  <div class="col-lg-6">
                    <header class="page-header">
                      <h1>
                        Informativos
                      </h1>
                      <p class="lead">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt
                      </p>
                    </header>
                  </div>
                  <div class="col-lg-6">
                    <button data-toggle="modal" data-target="#config-informativos" class="btn btn-inverse btn-rounded" type="button">
                      <i class="fa fa-cog" aria-hidden="true"></i> Configurar informativos
                    </button>
                  </div>
                </div>
                <div class="accordion-lg accordion" id="news" role="tablist" aria-multiselectable="true">
                  <div class="card card-lg">
                    <a data-toggle="collapse" data-parent="#news" href="#badgeOne" aria-expanded="false" aria-controls="badgeOne" class="card-header collapsed" role="tab" id="headingOne">
                      <div class="heading">
                        <h3 class="title">
                          Sed ut perspiciatis unde omnis iste natus error?
                        </h3 class="title">
                        <p>
                          10/05/2017 às 18h30. Via Normas Legais.
                        </p>
                      </div>
                    </a>
                    <div id="badgeOne" class="collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false">
                      <div class="card-block">
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <p>
                          Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
                        </p>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <p>
                          <a href="#" target="_blank">
                            <small>
                              Fonte: normaslegais.com.br/noticias
                            </small>
                          </a>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="card card-lg">
                    <a data-toggle="collapse" data-parent="#news" href="#badgeTwo" aria-expanded="true" aria-controls="badgeTwo" class="card-header collapsed" role="tab" id="headingTwo">
                      <div class="heading">
                        <h3 class="title">
                          Nulla elit orci, pretium in ligula a, iaculis tristique mauris. Maecenas efficitur mauris?
                        </h3 class="title">
                        <p>
                          10/05/2017 às 18h30. Via Normas Legais.
                        </p>
                      </div>
                    </a>
                    <div id="badgeTwo" class="collapse show" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="true">
                      <div class="card-block">
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <p>
                          Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
                        </p>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <p>
                          <a href="#" target="_blank">
                            <small>
                              Fonte: normaslegais.com.br/noticias
                            </small>
                          </a>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="card card-lg">
                    <a data-toggle="collapse" data-parent="#news" href="#badgeThree" aria-expanded="false" aria-controls="badgeThree" class="card-header collapsed" role="tab" id="headingThree">
                      <div class="heading">
                        <h3 class="title">
                          Sed ut perspiciatis unde omnis iste natus error?
                        </h3 class="title">
                        <p>
                          10/05/2017 às 18h30. Via Normas Legais.
                        </p>
                      </div>
                    </a>
                    <div id="badgeThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
                      <div class="card-block">
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <p>
                          Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
                        </p>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <p>
                          <a href="#" target="_blank">
                            <small>
                              Fonte: normaslegais.com.br/noticias
                            </small>
                          </a>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="card card-lg">
                    <a data-toggle="collapse" data-parent="#news" href="#badgeFour" aria-expanded="false" aria-controls="badgeFour" class="card-header collapsed" role="tab" id="headingFour">
                      <div class="heading">
                        <h3 class="title">
                          Sed ut perspiciatis unde omnis iste natus error?
                        </h3 class="title">
                        <p>
                          10/05/2017 às 18h30. Via Normas Legais.
                        </p>
                      </div>
                    </a>

                    <div id="badgeFour" class="collapse" role="tabpanel" aria-labelledby="headingFour" aria-expanded="false">
                      <div class="card-block">
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <p>
                          Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
                        </p>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <p>
                          <a href="#" target="_blank">
                            <small>
                              Fonte: normaslegais.com.br/noticias
                            </small>
                          </a>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="card card-lg">
                    <a data-toggle="collapse" data-parent="#news" href="#badgeFive" aria-expanded="false" aria-controls="badgeFive" class="card-header collapsed" role="tab" id="headingFive">
                      <div class="heading">
                        <h3 class="title">
                          Sed ut perspiciatis unde omnis iste natus error?
                        </h3 class="title">
                        <p>
                          10/05/2017 às 18h30. Via Normas Legais.
                        </p>
                      </div>
                    </a>
                    <div id="badgeFive" class="collapse" role="tabpanel" aria-labelledby="headingFive" aria-expanded="false">
                      <div class="card-block">
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <p>
                          Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
                        </p>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <p>
                          <a href="#" target="_blank">
                            <small>
                              Fonte: normaslegais.com.br/noticias
                            </small>
                          </a>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
						</div>
					</div>
        </div>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>

    <div class="modal fade" id="config-informativos" tabindex="-1" role="dialog" aria-labelledby="uploadLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="uploadLabel">
              Configuração dos Informativos
            </h5>
          </div>
          <div class="modal-body">
            <p class="mb-4">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.
            </p>
            <form action="">
              <div class="form-group row">
                <label class="col-sm-4 col-form-label" for="lorem">Fonte Lorem</label>
                <div class="col-sm-8">
                  <select class="form-control custom-select" id="lorem">
                    <option>Exibir</option>
                    <option>Não Exibir</option>
                  </select>
                </div>
              </div>
               <div class="form-group row">
                <label class="col-sm-4 col-form-label" for="ipsum">Fonte Ipsum</label>
                <div class="col-sm-8">
                  <select class="form-control custom-select" id="ipsum">
                    <option>Exibir</option>
                    <option>Não Exibir</option>
                  </select>
                </div>
              </div>
               <div class="form-group row">
                <label class="col-sm-4 col-form-label" for="darnet">Fonte Darnet</label>
                <div class="col-sm-8">
                  <select class="form-control custom-select" id="darnet">
                    <option>Exibir</option>
                    <option>Não Exibir</option>
                  </select>
                </div>
              </div>
              
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-success">Concluir</button>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
