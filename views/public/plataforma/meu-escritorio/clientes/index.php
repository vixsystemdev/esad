<?php
	 // General settings (relative path only here)
	 require ABSPATH . '/views/_includes/config.php';

	 $page = "meuescritorio";
?>

<!DOCTYPE html>
<html>
	<head>
		<?php require ABSPATH . '/views/_includes/metadata.php'; ?>
		<title>Contatos | Meu Escritório | esad</title>
		<?php require ABSPATH . '/views/_includes/styles.php'; ?>
	</head>
	<body>
		<div class="wrapper">
			<div class="layout-navbar layout-fluid <!--?php echo $template; ?->">
				<header>
					<?php require ABSPATH . "/views/_includes/navbar-fluid-plataforma.php"; ?>
				</header>
				<div class="wrapper-main">
					<div class="wrapper-content">
						<div class="container">
							<section>
								<div class="row">
									<div class="col-lg-6">
										<header class="page-header">
											<h1>
												Clientes
											</h1>
											<p class="lead">
												Aqui podem ser visualizados apenas seus clientes. Ao clicar sobre um cliente é possível visualizar os processos que você o patrocina e, além disso, é possível enviar um feedback para o e-mail cadastrado.
											</p>
										</header>
										<form role="form" method="post" enctype="multipart/form-data" autocomplete="off">
                                          <div class="form-group">
                                            <div class="input-group">
                                              <input name="cliente" id="cliente" type="text" class="form-control" placeholder="Localizar cliente">
                                              <span class="input-group-btn">
                                                <button class="btn btn-secondary" type="button" onclick="buscar()">
                                                  <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                              </span>
                                            </div>
                                          </div>
                                        </form>
									</div>
									<div class="col-lg-6">
                                    
                                    	<a href="<?php echo HOME_URI; ?>/plataforma/clientes/feeds/" class="text-default"> <i class="btn btn-inverse btn-rounded">Enviar feedback</i>  </a>                                                          
									<button data-toggle="modal" id="feedclientes" data-target="#enviar-feedsback" type="button" class="btn btn-inverse btn-rounded" data-placement="top" title="Enviar Feedback" style="display:none;"><i class="btn btn-inverse btn-rounded"></i></button>
                                    
										
									</div>
								</div>
								<div class="card card-table">
									<div class="card-header">
										<div class="row">
											<div class="col-6">
												<h3 class="title text-left">
													Relação de clientes
												</h3>
											</div>
											<div class="col-6">
												<a href="<?php echo HOME_URI; ?>/plataforma/clientes/" class="pull-right">
													<small>
														Exibir apenas Clientes
													</small>
												</a>
											</div>
										</div>
									</div>
									<div class="card-block">
										<div class="table table-responsive">
											<table class="table table-striped table-hover">
												<tbody>
                                                    <?php $lista = $modelo_clientes->listar_clientes();
													  foreach($lista as $clientes):?>
													<tr>
														<td class="text-left text-nowrap">
															<a href="#">
																<img src="<?php echo HOME_URI; ?>/view/assets/esad/temp/profile-leonardo.jpg" class="pull-left rounded-circle hidden-md-down" alt="Perfil">
																<div>
																	<?php echo $clientes['nome']?>
																	<small class="text-uppercase">
																		<?php echo $clientes['empresa']?>
																	</small>
																</div>
															</a>
														</td>
														<td class="text-nowrap hidden-sm-down">
															<a href="tel:<?php echo $clientes['celular']?>">
																<small class="text-muted">
																	<?php echo $clientes['celular']?>
																</small>
															</a>
														</td>
														<td class="text-nowrap hidden-lg-down">
															<a href="tel:<?php echo $clientes['telefone']?>">
																<small class="text-muted">
																	<?php echo $clientes['telefone']?>
																</small>
															</a>
														</td>
														<td class="text-nowrap hidden-sm-down">
															<a href="mailto:<?php echo $clientes['email']?>">
																<small class="text-muted">
																	<?php echo $clientes['email']?>
																</small>
															</a>
														</td>
														<td> 
                                                        	<a href="<?php echo HOME_URI; ?>/plataforma/clientes/feed/<?php echo $clientes['id']?>" class="text-default"> <i class="fa fa-envelope-o"></i> </a>                                                          
															<button data-toggle="modal" id="feedcliente" data-target="#enviar-feedback" type="button" class="text-default" data-placement="top" title="Enviar Feedback" style="display:none;">
																<i class="fa fa-envelope-o"></i>
															</button>
                                                            
                                                             <a href="<?php echo HOME_URI; ?>/plataforma/clientes/id/<?php echo $clientes['id']?>" class="text-default"> <i class="fa fa-user"></i> </a>
															<button data-toggle="modal" id="idcliente" data-target="#cadastro-cliente" type="button" class="text-default" data-placement="top" title="Informações do Contato" style="display:none;">
																<i class="fa fa-user"></i>
															</button>
                                                            <a href="<?php echo HOME_URI; ?>/plataforma/clientes/pro/<?php echo $clientes['id']?>" class="text-default"> <i class="fa fa-file-text-o"></i> </a>
															<button data-toggle="modal" id="procliente" data-target="#processos-cliente" type="button" class="text-default" data-placement="top" title="Processos Associados" style="display:none;">
																<i class="fa fa-file-text-o"></i>
															</button>
                                                                                                                            
                                                                <a href="<?php echo HOME_URI; ?>/plataforma/clientes/del/<?php echo $clientes['id']?>" class="text-danger"> <i class="fa fa-trash-o"></i> </a>
																<button data-toggle="modal" id="delcliente" data-target="#excluir-cliente" type="button" class="text-danger" data-placement="top" title="Excluir" style="display:none;">
                                                                
															</button>
                                                            
                                                           
														</td>
                                                        
													</tr>
                                                    <?php endforeach ?>
													

												</tbody>
											</table>
										</div>
										<nav class="pagination" aria-label="table-pagination">
											<div class="row">
												<div class="col-6">
													<small class="text-muted">
														1 - 5 de 10
													</small>
												</div>
												<div class="col-6">
													<ul class="pagination-links float-right">
														<li class="page-item">
														  <a class="page-link" href="#">
														    <i class="fa fa-chevron-left" aria-hidden="true"></i>
														  </a>
														</li>
														<li class="page-item">
														  <a class="page-link active" href="#">1</a>
														</li>
														<?php /*
														<li class="page-item">
														  <a class="page-link" href="#">2</a>
														</li>
														<li class="page-item">
														  <a class="page-link" href="#">3</a>
														</li>
														*/ ?>
														<li class="page-item">
														  <a class="page-link" href="#">
														    <i class="fa fa-chevron-right" aria-hidden="true"></i>
														  </a>
														</li>
													</ul>
												</div>
											</div>
										</nav>
									</div>
								</div>
							</section>
						</div>
					</div>
        </div>
      </div>
    </div>
    
    
    
    <?php require ABSPATH .'/views/_includes/scripts.php'; ?>
    <?php require ABSPATH .'/views/_includes/modal/_cliente.php'; ?>
    
    <script>
		var k = window.location.toString().indexOf("feed") > 0;
		if (k == true){
			$( "#feedcliente" ).click();
		}
		
		var w = window.location.toString().indexOf("feeds") > 0;
		if (w == true){
			$( "#feedclientes" ).click();
		}		
    
    	var x = window.location.toString().indexOf("id") > 0;
		if (x == true){
			$( "#idcliente" ).click();
		}
		
		var y = window.location.toString().indexOf("del") > 0;
		if (y == true){
			$( "#delcliente" ).click();
		}
        
        var y = window.location.toString().indexOf("pro") > 0;
		if (y == true){
			$( "#procliente" ).click();
		}	
		
		function buscar(){								
			
			alert('Julierelm Martins de Christo')
			busca = $("#cliente").val();			
			window.location.href = "<?php echo HOME_URI?>/plataforma/clientes/busca/" + busca;
					
		}
    
    </script>
    
  </body>
</html>
