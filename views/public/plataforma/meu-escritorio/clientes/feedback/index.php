<?php
	 // General settings (relative path only here)
	 require ABSPATH . '/views/_includes/config.php';

	 $page = "meuescritorio";
?>

<!DOCTYPE html>
<html>
	<head>
		<?php require ABSPATH . '/views/_includes/metadata.php'; ?>
		<title>Feedback para Cliente | Meu Escritório | esad</title>
		<?php require ABSPATH . '/views/_includes/styles.php'; ?>
	</head>
	<body>
    	
		<div class="wrapper">
			<div class="layout-navbar layout-fluid <!--?php echo $template; ?-->">
				<header>
					<?php require ABSPATH . "/views/_includes/navbar-fluid-plataforma.php"; ?>
				</header>
				<div class="wrapper-main">
                	<?php $modelo_feedbacks->selecionar_cliente(); ?>
					<div class="wrapper-content">
						<div class="container">
							<section>
								<div class="row">
									<div class="col-lg-6">
										<header class="page-header">
											<h1>
												Enviar Feedback
											</h1>
											<p class="lead">
												Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt
											</p>
										</header>
									</div>
									<div class="col-lg-6">
										<a href="<?php echo HOME_URI?>/plataforma/processox/id/<?php echo chk_array($modelo_feedbacks->form_data, 'pro.id'); ?>" class="btn btn-inverse btn-rounded">
											<i class="fa fa-file-text-o" aria-hidden="true"></i>
											Resumo do Processo
										</a>
									</div>
								</div>
								<div class="card card-lg">
                                	
									<div class="card-header mb-4">
										<h3 class="title">
                                        	Feedback para <?php echo chk_array($modelo_feedbacks->form_data, 'nome'); ?>
											
										</h3>
									</div>
									<div class="card-block">
										<div class="row">
											<div class="col-md-2 col-lg-1">
												<img class="img-fluid rounded-circle hidden-sm-down" src="<?php echo HOME_URI?>/views/public/assets/esad/temp/profile-leonardo.jpg" alt="Foto">
											</div>
											<div class="col-md-10 col-lg-11">
												<form action="">
													<textarea class="form-control" name="" id="" cols="30" rows="10" placeholder="Digite sua mensagem"></textarea>
													<button class="btn btn-success btn-submit mt-4 mb-4">
														Enviar feedback
													</button>
												</form>
											</div>
										</div>
									</div>
								</div>
								<div class="card card-lg mt-5 mb-6">
									<div class="card-header mb-4">
										<h3 class="title">
											Histórico de Feedbacks
										</h3>
									</div>
									<div class="card-block">
                                    
                                    	<?php $lista_feedback = $modelo_feedbacks->selecionar_feddbacks();
											foreach($lista_feedback as $feedbacks):?>
                                            <div class="row">
                                                <div class="col-md-2 col-lg-1">
                                                    <img class="img-fluid rounded-circle hidden-sm-down" src="<?php echo HOME_URI?>/views/public/assets/esad/temp/profile-leonardo.jpg" alt="Foto">
                                                </div>
    
                                                <div class="col-md-10 col-lg-11 text-left">
                                                    <p>
                                                        <strong>
                                                            Enviado em <?php $envio = strtotime($feedbacks['data_feedback']);
																			$datamensagem = date("d/m/Y", $envio);
																			$horamensagem = date("H:i", $envio);
																	echo $datamensagem." as ".$horamensagem ?> por Felipe <?php echo $feedbacks['nome']?>
                                                        </strong>
                                                    </p>
                                                    <p>
                                                        <?php echo $feedbacks['comentario']?>
                                                    </p>
                                                </div>
                                            </div>
                                            <hr>
										<?php endforeach ?>
								</div>
							</section>
						</div>
					</div>
        </div>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>
  </body>
</html>
