<?php
	 // General settings (relative path only here)
	  require ABSPATH . '/views/_includes/config.php';

	 $page = "meuescritorio";
	 $modelo_atividades->apaga_atividade();
?>

<!DOCTYPE html>
<html>
	<head>
		<?php require ABSPATH . '/views/_includes/metadata.php'; ?>
		<title>Atividades | Meu Escritório | esad</title>
		<?php require ABSPATH . '/views/_includes/styles.php'; ?>
	</head>
	<body>
		<div class="wrapper">
			<div class="layout-navbar layout-fluid <!--?php echo $template; ?-->">
				<header>
					<?php require ABSPATH . "/views/_includes/navbar-fluid-plataforma.php"; ?>
				</header>

				<div class="wrapper-main">
					<div class="wrapper-content">
						<div class="container">
							<section>
								<div class="row">
									<div class="col-lg-6">
										<header class="page-header">
											<h1>
												Atividades
											</h1>
											<p class="lead">
												Aqui estão todas as atividades cadastradas por você, caso queira visualizá-las no calendário por material, clique nas pastas abaixo.
											</p>
										</header>
                                            <form role="form" method="post" enctype="multipart/form-data" autocomplete="off">
                                              <div class="form-group">
                                                <div class="input-group">
                                                  <input name="processo" id="processo" type="text" class="form-control" placeholder="Localizar processo">
                                                  <span class="input-group-btn">                                                  
                                                    <button class="btn btn-secondary" type="button" onclick="buscar()">
                                                      <i class="fa fa-search" aria-hidden="true"></i>
                                                    </button>
                                                  </span>
                                                </div>
                                              </div>
                                            </form>
									</div>
									<div class="col-lg-6">
                                    
                                    	<a href="<?php echo HOME_URI; ?>/plataforma/atividades/atividade1" class="btn btn-inverse btn-rounded">
											<i class="fa fa-plus" aria-hidden="true"></i>
											Adicionar atividade
										</a>
										
									</div>
								</div>
								<div class="row">
									<div class="col-xl-3 col-lg-4 mb-4">
										<ul class="nav flex-column nav-pills">
                                        
											<?php $lista = $modelo_atividades->listar_atividades();
                                                foreach($lista as $processos):?>                                                
                                                    <li class="nav-item text-left">
                                                        <a class="nav-link" href="<?php echo HOME_URI; ?>/plataforma/atividades/materia/<?php echo $processos['id']?>">
                                                            <i class="fa fa-folder" aria-hidden="true"></i>
                                                           <?php echo $processos['atividade']?>
                                                        </a>
                                                    </li>												
                                            <?php endforeach ?>
                                        
											
										</ul>
									</div>
									<div class="col-xl-9 col-lg-8">
										<div class="card card-table">
											<div class="card-header">
												<div class="row">
													<div class="col-6">
														<h3>
															Atividades pendentes
														</h3>
													</div>
													<div class="col-6">
														<a href="#" class="pull-right">
															Exibir concluídas
														</a>
													</div>
												</div>
											</div>
											<div class="card-block">
												<div class="table table-responsive">
													<table class="table no-border table-hover">
														<tbody>
                                                        	<?php $lista = $modelo_atividades->listar_atividade();
																foreach($lista as $atividades):?>
                                                                
                                                                <tr>
                                                                    <td>
                                                                        <div class="checkbox">
                                                                          <label for="">
                                                                            <input type="checkbox" value="0">
                                                                            <span class="text-muted text-left pt-0">
                                                                                 <?php echo $atividades['atividade']?>
                                                                                <small>
                                                                                     <?php echo $atividades['nome']?>
                                                                                </small>
                                                                               </span>
                                                                          </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <span class="text-muted">
                                                                            <small>
                                                                            	<?php 
																					$envio = strtotime($atividades['data']);
																					$datamensagem = date("d/m/Y", $envio);
																					$horamensagem = date("H:i", $envio);
																					echo $datamensagem." ÀS ".$horamensagem
																				?>
                                                                            </small>
                                                                        </span>
                                                                    </td>
                                                                    <td>
                                                                    
                                                                    <a href="<?php echo HOME_URI; ?>/plataforma/atividades/del/<?php echo $atividades['id']?>" class="text-danger"> <i class="fa 				                                                                        fa-trash-o"></i> </a>
																		<button data-toggle="modal" id="delatividade" data-target="#excluir-atividade" type="button"
                                                                         class="text-danger" data-placement= "top" title="Excluir" style="display:none;">                                                                                                                                           
                                                                    </td>
																</tr>
                                                                
                                                                 										
															<?php endforeach ?>
                                                        
                                                        
															
														</tbody>
													</table>
												</div>
												<nav class="pagination" aria-label="table-pagination">
													<div class="row">
														<div class="col-6">
															<small class="text-muted">
																1 - 5 de 10
															</small>
														</div>
														<div class="col-6">
															<ul class="pagination-links float-right">
																<li class="page-item">
																  <a class="page-link" href="#">
																    <i class="fa fa-chevron-left" aria-hidden="true"></i>
																  </a>
																</li>
																<li class="page-item">
																  <a class="page-link active" href="#">1</a>
																</li>
																<?php /*
																<li class="page-item">
																  <a class="page-link" href="#">2</a>
																</li>
																<li class="page-item">
																  <a class="page-link" href="#">3</a>
																</li>
																*/ ?>
																<li class="page-item">
																  <a class="page-link" href="#">
																    <i class="fa fa-chevron-right" aria-hidden="true"></i>
																  </a>
																</li>
															</ul>
														</div>
													</div>
												</nav>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
					</div>
        </div>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>

    <div class="modal fade" id="nova-atividade" tabindex="-1" role="dialog" aria-labelledby="nova-atividade" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title">
		          Nova atividade
		        </h5>
		      </div>
		      <div class="modal-body">
		        <p class="mb-4">
		          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.
		        </p>
		        <form action="">
		          <div class="form-group row">
		            <label class="col-sm-4 col-form-label" for="materia">Matéria</label>
		            <div class="col-sm-8">
		              <select class="form-control custom-select" id="materia">
                       	<?php $lista = $modelo_atividades->listar_atividades();
								foreach($lista as $processos):?>
								<option><?php echo $processos['atividade']?></option>
                        <?php endforeach ?>
                        
		            
		              </select>
		            </div>
		          </div>
		          <div class="form-group row">
		            <label class="col-sm-4 col-form-label" for="cliente">Cliente</label>
		            <div class="col-sm-8">
		              <select class="form-control custom-select" id="cliente">
                      	<?php $lista = $modelo_atividades->listar_contatos();
						  	   foreach($lista as $processos):?>
								<option><?php echo $processos['nome']?></option>
                        <?php endforeach ?>
                      		                
		              </select>
		            </div>
		          </div>
		          <div class="form-group row">
		            <label class="col-sm-4 col-form-label" for="nProcesso">Nº do Processo:</label>
		            <div class="col-sm-8">
		              <select class="form-control custom-select" id="nProcesso">
                      
                      		<?php $lista = $modelo_atividades->selecionar_processos();
						  	   foreach($lista as $processos):?>
								<option><?php echo $processos['num_processo']?></option>
                       		<?php endforeach ?>
                      
                      	
                      
		              
		              </select>
		            </div>
		          </div>
		          <div class="form-group row">
		            <label class="col-sm-4 col-form-label" for="atividade">Atividade</label>
		            <div class="col-sm-8">
		              <input type="text" class="form-control" id="atividade" name="atividade" placeholder="">
		            </div>
		          </div>
		        </form>
		      </div>
		      <div class="modal-footer">
		        <a href="<?php echo HOME_URI . '/plataforma/atividades' ?>" class="btn btn-default">Cancelar</a>
		        <button type="button" class="btn btn-success" data-dismiss="modal">Concluir</a>
		      </div>
		    </div>
		  </div>
		</div>
        
        
        
        <div class="modal fade" id="excluir-atividade" tabindex="-1" role="dialog" aria-labelledby="excluir-atividade" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="uploadLabel">
                  Excluir Atividade
                </h5>
              </div>
              <div class="modal-body">
                <p class="mb-4">
                  Tem certeza que deseja excluir este atividade?
                </p>        
                
              </div>
              <div class="modal-footer">
                <a href="<?php echo HOME_URI . '/plataforma/atividades' ?>" class="btn btn-default">Não</a>
                <a href="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '/confirma'; ?> " class="btn btn-default">Sim, tenho certeza!</a>
              </div>
            </div>
          </div>
        </div>
        
        
        <script>
        		var y = window.location.toString().indexOf("atividade1") > 0;
				if (y == true){
					$( "#nova-atividade" ).modal('show');
				}
				
				var x = window.location.toString().indexOf("del") > 0;
				if (x == true){
					$( "#delatividade" ).click();
				}
				
				function buscar(){
					
					busca = $("#processo").val();
					window.location.href = "<?php echo HOME_URI?>/plataforma/atividades/busca/" + busca;
					
				}
				
        </script>
  </body>
</html>
