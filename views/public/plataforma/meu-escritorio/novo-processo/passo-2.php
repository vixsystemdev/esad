<?php
	 // General settings (relative path only here)
	require ABSPATH . '/views/_includes/config.php';

	 $page = "meuescritorio";
?>

<!DOCTYPE html>
<html>
	<head>
		<?php require ABSPATH . '/views/_includes/metadata.php'; ?>
		<title>Novo Processo | Meu Escritório | esad</title>
		<?php require ABSPATH . '/views/_includes/styles.php'; ?>
	</head>
	<body>
		<div class="wrapper">
			<div class="layout-navbar layout-fluid <!--?php echo $template; ?-->">
				<header>
					<?php require ABSPATH . "/views/_includes/navbar-fluid-plataforma.php"; ?>
				</header>
				<div class="wrapper-main">
					<div class="wrapper-content">
						<div class="container">
							<section>
								<header class="page-header">
									<h1>
										Processo 103202.3202032.1021
									</h1>
									<p class="lead my-4">
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit, <br class="hidden-xs-down">
										sed diam nonummy nibh euismod tincidunt.
		              </p>
								</header>
								<form class="no-border" action="<?php echo HOME_URI?>/plataforma/processox/">
									<div class="my-5">
										<h2>
											Informações do processo:
										</h2>
										<p>
											Lorem ipsum dolor sit amet, consectetuer adipiscing elit, <br class="hidden-xs-down">
											sed diam nonummy nibh euismod tincidunt.
										</p>
									</div>
	                <div class="form-group row">
	                  <label for="parte-ativa" class="col-md-4 col-form-label">
	                    <span>
	                      Parte Ativa:
	                    </span>
	                    <small class="text-muted">
	                    	Informação obrigatória
	                    </small>
	                  </label>
	                  <div class="col-md-8">
	                  	<input type="text" class="form-control" id="parte-ativa" name="parte-ativa" placeholder="">
	                  	<button data-toggle="modal" data-target="#associar-contato" type="button" class="mt-2 btn btn-sm btn-info">
												<i class="fa fa-user" aria-hidden="true"></i>
												Associar contato
											</button>
	                  </div>
	                </div>
	                <div class="form-group row">
	                  <label for="parte-passiva" class="col-md-4 col-form-label">
	                    <span>
	                      Parte Passiva
	                    </span>
	                    <small class="text-muted">
	                    	Informação obrigatória
	                    </small>
	                  </label>
	                  <div class="col-md-8">
	                  	<input type="text" class="form-control" id="parte-passiva" name="parte-passiva" placeholder="">
	                  	<button data-toggle="modal" data-target="#associar-contato" type="button" class="mt-2 btn btn-sm btn-info">
												<i class="fa fa-user" aria-hidden="true"></i>
												Associar contato
											</button>
	                  </div>
	                </div>
	                <hr>
	                <div class="form-group row">
	                  <label for="localizacao" class="col-md-4 col-form-label">
	                    <span>
	                      Localização
	                    </span>
	                    <small class="text-muted">
	                    	Informação obrigatória
	                    </small>
	                  </label>
	                  <div class="col-md-8">
	                  	<input type="text" class="form-control" id="localizacao" name="localizacao" placeholder="">
	                  </div>
	                </div>
	                <div class="form-group row">
	                  <label for="materia" class="col-md-4 col-form-label">
	                    <span>
	                      Matéria
	                    </span>
	                    <small class="text-muted">
	                    	Informação obrigatória
	                    </small>
	                  </label>
	                  <div class="col-md-8">
                      
                          <select class="form-control custom-select" id="">
                          			<input type="text" class="form-control" id="materia" name="materia" placeholder="">
                                 
                                                            
                        </select>
                      	
	                  	
	                  </div>
	                </div>
	                <div class="form-group row">
	                  <label for="fase" class="col-md-4 col-form-label">
	                    <span>
	                      Fase
	                    </span>
	                    <small class="text-muted">
	                    	Informação obrigatória
	                    </small>
	                  </label>
	                  <div class="col-md-8">
	                  	<input type="text" class="form-control" id="fase" name="fase" placeholder="">
	                  </div>
	                </div>
	                <hr>
	                <div class="form-group row">
	                  <label for="valor-causa" class="col-md-4 col-form-label">
	                    <span>
	                      Valor da Causa:
	                    </span>
	                    <small class="text-muted">
	                    	Informação opcional
	                    </small>
	                  </label>
	                  <div class="col-md-8">
	                  	<input type="text" class="form-control" id="valor-causa" name="valor-causa" placeholder="">
	                  </div>
	                </div>
	                <hr>
									<div class="mt-9 mb-5">
										<h2>
											Informações sobre a contratação:
										</h2>
										<p>
											Lorem ipsum dolor sit amet, consectetuer adipiscing elit, <br class="hidden-xs-down">
											sed diam nonummy nibh euismod tincidunt.
										</p>
									</div>
									<div class="form-group row">
	                  <label for="data-contratacao" class="col-md-4 col-form-label">
	                    <span>
	                      Data da Contratação
	                    </span>
	                    <small class="text-muted">
	                    	Informação opcional.
	                    </small>
	                  </label>
	                  <div class="col-md-8">
	                  	<input type="date" class="form-control" placeholder="Ex.: dd/mm/aaaa" data-mask="00/00/0000" maxlength="10" autocomplete="off" id="data-contratacao" name="data-contratacao" placeholder="">
	                  </div>
	                </div>
	                <hr>
									<div class="form-group row">
	                  <label for="honorarios-exito" class="col-md-4 col-form-label">
	                    <span>
	                      Honorários de êxito:
	                    </span>
	                    <small class="text-muted">
	                    	Informação opcional.
	                    </small>
	                  </label>
	                  <div class="col-md-8">
	                  	<div class="input-group">
		                    <span class="input-group-addon">
		                      R$
		                    </span>
		                    <input type="text" class="form-control" id="honorarios-exito" name="honorarios-exito">
		                  </div>
		                  <div class="mt-2">
			                  <label for="honorarios-exito-aplicavel">
			                    <input type="checkbox" id="honorarios-exito-aplicavel">
			                    <span class="text-muted"><small>Não aplicável</small></span>
			                  </label>
			                </div>
	                  </div>
	                </div>
	                <div class="form-group row">
	                  <label for="honorarios-sucumbenciais" class="col-md-4 col-form-label">
	                    <span>
	                      Honorários sucumbenciais:
	                    </span>
	                    <small class="text-muted">
	                    	Informação opcional.
	                    </small>
	                  </label>
	                  <div class="col-md-8">
	                  	<div class="input-group">
		                    <span class="input-group-addon">
		                      R$
		                    </span>
		                    <input type="text" class="form-control" id="honorarios-sucumbenciais" name="honorarios-sucumbenciais">
		                  </div>
	                  	<div class="mt-2">
			                  <label for="honorarios-sucumbenciais-aplicavel">
			                    <input type="checkbox" id="honorarios-sucumbenciais-aplicavel">
			                    <span class="text-muted"><small>Não aplicável</small></span>
			                  </label>
			                </div>
	                  </div>
	                </div>
	                <hr>
	                <button class="btn btn-success btn-rounded btn-submit">
	                	<i class="fa fa-check" aria-hidden="true"></i>
	                	Concluir Cadastro
	                </button>
								</form>
							</section>
						</div>
					</div>
        </div>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>

    <!--?php require ABSPATH . '/views/_includes/modal/_contato.php'; ?-->
   <?php require ABSPATH . '/views/_includes/modal/_associar.php'; ?>


  </body>
</html>
