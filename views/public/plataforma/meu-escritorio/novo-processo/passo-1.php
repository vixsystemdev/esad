<?php
	 // General settings (relative path only here)
	require ABSPATH . '/views/_includes/config.php';

	 $page = "meuescritorio";
?>

<!DOCTYPE html>
<html>
	<head>
		<?php require ABSPATH . '/views/_includes/metadata.php'; ?>
		<title>Novo Processo | Meu Escritório | esad</title>
		<?php require ABSPATH . '/views/_includes/styles.php'; ?>
	</head>
	<body>
		<div class="wrapper">
			<div class="layout-navbar layout-fluid <!--?php echo $template; ?-->">
				<header>
					<?php require ABSPATH . "/views/_includes/navbar-fluid-plataforma.php"; ?>
				</header>
				<div class="wrapper-main">
					<div class="wrapper-content">
						<div class="container">
							<section>
								<header class="page-header">
									<h1>
										Novo Processo
									</h1>
									<p class="lead my-4">
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit, <br class="hidden-xs-down">
										sed diam nonummy nibh euismod tincidunt.
		              </p>
								</header>
								<form class="no-border mt-6" action="<?php echo HOME_URI?>/plataforma/avancaprocesso">
									<div class="form-group row">
	                  <label for="num-processo" class="col-md-4 col-form-label">
	                    <span>
	                      Nº do processo:
	                    </span>
	                    <small class="text-muted">
	                    	Informação obrigatória
	                    </small>
	                  </label>
	                  <div class="col-md-8">
	                  	<input type="text" class="form-control" id="num-processo" name="num-processo" placeholder="">
	                  </div>
	                </div>
	                <button class="btn btn-success btn-rounded btn-submit">
	                	<i class="fa fa-check" aria-hidden="true"></i> Avançar
	                </button>
								</form>
							</section>
						</div>
					</div>
        </div>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>
  </body>
</html>
