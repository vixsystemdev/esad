<?php
	 // General settings (relative path only here)
	 require ABSPATH . '/views/_includes/config.php';

	 $page = "meuescritorio";
?>

<!DOCTYPE html>
<html>
	<head>
		<?php require ABSPATH . '/views/_includes/metadata.php'; ?>
		<title>Meus processos | Meu Escritório | esad</title>
		<?php require ABSPATH . '/views/_includes/styles.php'; ?>
	</head>
	<body>
		<div class="wrapper">
			<div class="layout-navbar layout-fluid <!--?php echo $template; ?-->">
				<header>
					<?php require ABSPATH . "/views/_includes/navbar-fluid-plataforma.php"; ?>
				</header>
				<div class="wrapper-main">
					<div class="wrapper-content">
						<div class="container">
							<section>
								<div class="row">
									<div class="col-lg-6">
										<header class="page-header">
											<h1>
												Meus Processos
											</h1>
											<p class="lead">
												Abaixo estão todos os processos cadastrados por você, se quiser realizar alguma busca utilize os filtros abaixo ou a digite o número do processo no campo abaixo.
											</p>
										</header>
										<form role="form" method="post" enctype="multipart/form-data" autocomplete="off">
                                          <div class="form-group">
                                            <div class="input-group">
                                              <input name="contato" id="processo" type="text" class="form-control" placeholder="Localizar processo">
                                              <span class="input-group-btn">
                                                <button class="btn btn-secondary" type="button" onclick="buscar()">
                                                  <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                              </span>
                                            </div>
                                          </div>
                                        </form>
									</div>
									<div class="col-lg-6">
										<a href="<?php echo HOME_URI?>/plataforma/novoprocesso" class="btn btn-inverse btn-rounded">
											<i class="fa fa-plus" aria-hidden="true"></i>
											Adicionar novo
										</a>
									</div>
								</div>

								<div class="card card-table">
									<div class="card-block">
										<div class="table-filter">
											<button class="btn btn-default btn-block hidden-lg-up" type="button" data-toggle="collapse" data-target="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter">
											  <i class="fa fa-list" aria-hidden="true"></i> Filtrar
											</button>
											<form action="" class="collapse" id="collapseFilter" aria-labelledby="collapseFilter">
												<div class="form-group">
													<select class="form-control custom-select" id="">
														<option value="" selected="selected" disabled="disabled" hidden>Todos clientes</option>
                                                        <?php $lista = $modelo_processos->selecionado_cliente();
													  	   foreach($lista as $processos):?>
															<option><?php echo $processos['nome']?></option>
                                                        <?php endforeach ?>
													</select>
												</div>

												<div class="form-group">
													<select class="form-control custom-select" id="">
														<option value="" selected="selected" disabled="disabled" hidden>Todas matérias</option>
                                                        <?php $lista = $modelo_processos->selecionado_materias();
													  	   foreach($lista as $processos):?>
															<option><?php echo $processos['atividade']?></option>
                                                        <?php endforeach ?>
														
													</select>
												</div>

												<div class="form-group">
													<select class="form-control custom-select" id="">
														<option value="" selected="selected" disabled="disabled" hidden>Todas fases</option>
														<option>Lorem ipsum</option>
													</select>
												</div>


												<div class="form-group">
													<select class="form-control custom-select" id="">
														<option value="" selected="selected" disabled="disabled" hidden>Qualquer ganho</option>
														<option>Lorem ipsum</option>
													</select>
												</div>

												<div class="form-group">
													<select class="form-control custom-select" id="">
														<option value="" selected="selected" disabled="disabled" hidden>Duração</option>
														<option>Lorem ipsum</option>
													</select>
												</div>

												<div class="form-group">
													<select class="form-control custom-select" id="">
														<option value="" selected="selected" disabled="disabled" hidden>Movimentação</option>
														<option>Lorem ipsum</option>
													</select>
												</div>
											</form>
										</div>
										<div class="table table-responsive">
											<table class="table table-striped table-hover">
												<thead>
													<tr>
														<th>Cliente</th>
														<th>Número do processo</th>
														<th class="hidden-sm-down">Matéria</th>
														<th class="hidden-sm-down">Fase</th>
														<th class="hidden-sm-down">Expectativa de ganho</th>
														<th></th>
													</tr>
												</thead>
												<tbody>
                                                	 <?php $lista = $modelo_processos->listar_processos();
													  	   foreach($lista as $processos):?>
                                                
													<tr>
														<td>
															<a href="<?php echo HOME_URI?>/plataforma/processox/id/<?php echo $processos['id']?>">
																<?php echo $processos['nome']?>
															</a>
														</td>
														<td>
															<a href="<?php echo HOME_URI?>/plataforma/processox/id/<?php echo $processos['id']?>">
                                                            	<?php echo $processos['num_processo']?>
															</a>
														</td>
														<td class="hidden-sm-down">
                                                        	<?php echo $processos['atividade']?>
														</td>
														<td class="hidden-sm-down">
															Conhecimento
														</td>
														<td class="hidden-sm-down">
                                                        
                                                        		<?php 
																		$exito = $processos['homorario_exito'];
																		$valor = $processos['valor_causar'];
																		$sucumbencia = $processos['homorario_sucumbencias'];
																		$somar = $exito + $valor + $sucumbencia;
																		echo 'R$	' . number_format($somar, 2, ',', '.')?>
                                                               
														</td>
														<td>
															<div class="dropdown">
																<button class="text-default" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	<i class="fa fa-ellipsis-v" aria-hidden="true"></i>
																</button>
																<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
																	<a class="dropdown-item" href="<?php echo HOME_URI?>/plataforma/processox/id/<?php echo $processos['id']?>">
																		Resumo
																	</a>
																	<a class="dropdown-item" href="<?php echo HOME_URI?>/plataforma/andamentos/id/<?php echo $processos['id']?>">
																		Andamento
																	</a>
																	<a class="dropdown-item" href="<?php echo HOME_URI?>/plataforma/atividades/id/<?php echo $processos['id']?>">
																		Atividades
																	</a>
																	<a class="dropdown-item" href="<?php echo HOME_URI?>/plataforma/processo/id/<?php echo $processos['id']?>">
																		Arquivos
																	</a>
																	<a class="dropdown-item" href="<?php echo HOME_URI?>/plataforma/contatos/">
																		Contatos
																	</a>
																	<a class="dropdown-item" href="/plataforma/meu-escritorio/clientes/feedback/id/<?php echo $processos['id']?>">
																		Feedback para Cliente
																	</a>
																</div>
															</div>
														</td>
													</tr>
                                                     <?php endforeach ?>
													
												</tbody>
											</table>
										</div>
										<nav class="pagination" aria-label="table-pagination">
											<div class="row">
												<div class="col-6">
													<p class="text-muted">
														1 - 5 de 5
													</p>
												</div>
												<div class="col-6">
													<ul class="pagination-links float-right">
														<li class="page-item">
														  <a class="page-link" href="#">
														    <i class="fa fa-chevron-left" aria-hidden="true"></i>
														  </a>
														</li>
														<li class="page-item">
														  <a class="page-link active" href="#">1</a>
														</li>
														<?php /*
														<li class="page-item">
														  <a class="page-link" href="#">2</a>
														</li>
														<li class="page-item">
														  <a class="page-link" href="#">3</a>
														</li>
														*/ ?>
														<li class="page-item">
														  <a class="page-link" href="#">
														    <i class="fa fa-chevron-right" aria-hidden="true"></i>
														  </a>
														</li>
													</ul>
												</div>
											</div>
										</nav>
									</div>
								</div>
							</section>
						</div>
					</div>
        </div>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>
    
    <script>
    
		function buscar(){
					
			busca = $("#processo").val();
			window.location.href = "<?php echo HOME_URI?>/plataforma/processos/busca/" + busca;
					
		}
	
    </script>
  </body>
</html>
