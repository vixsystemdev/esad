<?php
	 // General settings (relative path only here)
	 require ABSPATH . '/views/_includes/config.php';

	 $page = "meuescritorio";
?>

<!DOCTYPE html>
<html>
	<head>
		<?php require ABSPATH . '/views/_includes/metadata.php'; ?>
		<title>Processo | Meu Escritório | esad</title>
		<?php require ABSPATH . '/views/_includes/styles.php'; ?>
	</head>
	<body>
		<div class="wrapper">
			<div class="layout-navbar layout-fluid <!--?php echo $template; ?-->">
				<header>
					<?php require ABSPATH . "/views/_includes/navbar-fluid-plataforma.php"; ?>
				</header>
				<div class="wrapper-main">
                	<?php $modelo_processo_resumo->selecionar_processo_resumo(); ?>
					<div class="wrapper-content">
						<div class="container">
							<section>
								<header class="page-header">
									<h1>
                                    
										Processo <small>Nº <?php echo chk_array($modelo_processo_resumo->form_data, 'num_processo'); ?></small>
									</h1>
									<p class="lead">
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit, <br class="hidden-xs-down">
										sed diam nonummy nibh euismod tincidunt
									</p>
								</header>
								<div class="card mb-4">
									<div class="card-header">
										<h3 class="title text-left">
											Informações Gerais
										</h3>
									</div>
									<div class="card-block">
										<div class="row">
											<div class="col-lg-6">
												<div class="row mb-4">
													<div class="col-lg-4">
														<strong>
															Matéria:  
														</strong>
													</div>
													<div class="col-lg-8">
														<?php echo chk_array($modelo_processo_resumo->form_data, 'atividade'); ?>
													</div>
												</div>
												<div class="row mb-4">
													<div class="col-lg-4">
														<strong>
															Fase:
														</strong>
													</div>
													<div class="col-lg-8">
														Conhecimento
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="row mb-4">
													<div class="col-lg-4">
														<strong>
															Parte ativa:
														</strong>
													</div>
													<div class="col-lg-8">
														<p class="text-uppercase mb-0">
															<?php echo chk_array($modelo_processo_resumo->form_data, 'parte_ativa'); ?>
														</p>
													</div>
												</div>
												<div class="row mb-4">
													<div class="col-lg-4">
														<strong>
															Parte passiva:
														</strong>
													</div>
													<div class="col-lg-8">
														<p class="text-uppercase mb-0">
															<?php echo chk_array($modelo_processo_resumo->form_data, 'parte_passiva'); ?>
														</p>
														
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="card card-table mb-4" >
									<div class="card-header">
										<div class="row">
											<div class="col-6">
												<h3 class="title text-left">
													Andamentos recentes
												</h3>
											</div>
											<div class="col-6">
												<a href="<?php echo HOME_URI?>/plataforma/andamentos/" class="pull-right">
													Exibir todos
												</a>
											</div>
										</div>
									</div>
									<div class="card-block">
										<div class="table">
											<table class="table table-striped table-hover">
												<thead>
													<tr>
														<th>Data</th>
														<th>Andamento</th>
														<th>Localização</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>
															31/01/2017
														</td>
														<td>
															<a href="<?php echo HOME_URI?>/plataforma/andamentos/">
																Petição juntada aos autos
															</a>
														</td>
														<td>
															<span class="text-uppercase">
																Tribunal de Justiça - 2ª Câmara Cível
															</span>
														</td>
													</tr>
													<tr>
														<td>
															31/01/2017
														</td>
														<td>
															<a href="<?php echo HOME_URI?>/plataforma/andamentos/">
																Petição juntada aos autos
															</a>
														</td>
														<td>
															<span class="text-uppercase">
																Tribunal de Justiça - 2ª Câmara Cível
															</span>
														</td>
													</tr>
													<tr>
														<td>
															31/01/2017
														</td>
														<td>
															<a href="<?php echo HOME_URI?>/plataforma/andamentos/">
																Petição juntada aos autos
															</a>
														</td>
														<td>
															<span class="text-uppercase">
																Tribunal de Justiça - 2ª Câmara Cível
															</span>
														</td>
													</tr>
													<tr>
														<td>
															31/01/2017
														</td>
														<td>
															<a href="<?php echo HOME_URI?>/plataforma/andamentos/">
																Petição juntada aos autos
															</a>
														</td>
														<td>
															<span class="text-uppercase">
																Tribunal de Justiça - 2ª Câmara Cível
															</span>
														</td>
													</tr>
													<tr>
														<td>
															31/01/2017
														</td>
														<td>
															<a href="<?php echo HOME_URI?>/plataforma/andamentos/">
																Petição juntada aos autos
															</a>
														</td>
														<td>
															<span class="text-uppercase">
																Tribunal de Justiça - 2ª Câmara Cível
															</span>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="row mb-4">
									<div class="col-lg-6 mb-xs-4 mb-sm-4 mb-md-4">
										<div class="card card-table">
											<div class="card-header">
												<div class="row">
													<div class="col-6">
														<h3 class="title text-left">
															Atividades pendentes
														</h3>
													</div>
													<div class="col-6">
														<a href="/plataforma/atividades/" class="pull-right">
															Exibir todas
														</a>
													</div>
												</div>
											</div>
											<div class="card-block">
												<div class="table table-responsive">
													<table class="table no-border table-hover">
														<tbody>
															<tr>
																<td>
																	<div class="checkbox">
									                  <label for="">
									                    <input type="checkbox" value="0">
									                    <span class="text-muted text-left pt-0">
									                    	Diligência de terceiros
									                    	<small>
									                    		Loureiro & Diniz Sociedade de Advogados
										                    </small>
										                   </span>
									                  </label>
									                </div>
																</td>
																<td>
																	<span class="text-muted">
																		<small>
																			10/12/2017 ÀS 14h00
																		</small>
																	</span>
																</td>
															</tr>
															<tr>
																<td>
																	<div class="checkbox">
									                  <label for="">
									                    <input type="checkbox" value="0">
									                    <span class="text-muted text-left pt-0">
									                    	Diligência de terceiros
									                    	<small>
									                    		Loureiro & Diniz Sociedade de Advogados
										                    </small>
										                   </span>
									                  </label>
									                </div>
																</td>
																<td>
																	<span class="text-muted">
																		<small>
																			10/12/2017 ÀS 14h00
																		</small>
																	</span>
																</td>
															</tr>
															<tr>
																<td>
																	<div class="checkbox">
									                  <label for="">
									                    <input type="checkbox" value="0">
									                    <span class="text-muted text-left pt-0">
									                    	Diligência de terceiros
									                    	<small>
									                    		Loureiro & Diniz Sociedade de Advogados
										                    </small>
										                   </span>
									                  </label>
									                </div>
																</td>
																<td>
																	<span class="text-muted">
																		<small>
																			10/12/2017 ÀS 14h00
																		</small>
																	</span>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="card ">
											<div class="card-header">
												<h3 class="title text-left">
													Calendário
												</h3>
											</div>
											<div class="card-block">
												
											</div>
										</div>
									</div>
								</div>
								<div class="card card-table mb-4">
									<div class="card-header">
										<div class="row">
											<div class="col-6">
												<h3 class="title text-left">
													Arquivos do processo
												</h3>
											</div>
											<div class="col-6">
												<a href="<?php echo HOME_URI?>/plataforma/processo/" class="pull-right">
													Exibir pasta
												</a>
											</div>
										</div>
									</div>
									<div class="card-block">
										<div class="table table-responsive">
											<table class="table no-border table-hover">
												<thead class="no-border">
													<tr>
														<th class="text-nowrap">
															Processo <i class="fa fa-angle-down"></i>
														</th>
														<th class="text-nowrap">
															Tipo <i class="fa fa-angle-down"></i>
														</th>
														<th class="text-left text-nowrap">
															Última alteração <i class="fa fa-angle-down"></i>
														</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td class="text-left text-nowrap">
															<a href="#">
																<i class="fa fa-file-text-o fa-lg" aria-hidden="true"></i>
																arquivo do processo.pdf
															</a>
														</td>
														<td>
															Documento
														</td>
														<td class="text-left">
															10/05/2016 por Leonardo Fraga
														</td>
													</tr>
													<tr>
														<td class="text-left text-nowrap">
															<a href="#">
																<i class="fa fa-file-image-o fa-lg" aria-hidden="true"></i>
																documento do cliente.jpg
															</a>
														</td>
														<td>
															Imagem
														</td>
														<td class="text-left">
															10/05/2016 por Leonardo Fraga
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="card mb-4 card-expectativa">
									<div class="card-header row">
										<h3 class="title text-left">
											Expectativa de ganho
										</h3>
									</div>
									<div class="card-block">
										<div class="row">
											<div class="col-lg-6 heading">
												<p>
													Lorem ipsum dolor sit amet, consectetuer adipiscing elit, <br class="hidden-xs-down">sed diam nonummy nibh euismod tincidunt
												</p>
												<h4 class="value">
                                                		<?php 
															$exito = chk_array($modelo_processo_resumo->form_data, 'homorario_exito');
															$valor = chk_array($modelo_processo_resumo->form_data, 'valor_causar');
															$sucumbencia = chk_array($modelo_processo_resumo->form_data, 'homorario_sucumbencias');
															$somar = $exito+$valor+$sucumbencia;
															echo   'R$	' . number_format($somar, 2, ',', '.');?>
													
												</h4>
											</div>
											<div class="col-lg-6 data">
												<div class="row mt-3">
													<div class="col-md-5 col-lg-8">
														<p class="title">
															Honorários contratuais:
														</p>
													</div>
													<div class="col-md-7 col-lg-4">
														<p class="value">
															<?php 
															$real = chk_array($modelo_processo_resumo->form_data, 'homorario_exito');
															echo   'R$	' . number_format($real, 2, ',', '.');?>
														</p>
													</div>
												</div>
												<div class="row">
													<div class="col-md-5 col-lg-8">
														<p class="title">
															Percentual de êxito:
														</p>
													</div>
													<div class="col-md-7 col-lg-4">
														<p class="value">
                                                        <?php 
															$real = chk_array($modelo_processo_resumo->form_data, 'valor_causar');
															echo   'R$	' . number_format($real, 2, ',', '.');?>
														</p>
													</div>
												</div>
												<div class="row">
													<div class="col-md-5 col-lg-8">
														<p class="title">
															Honorários sucumbenciais:
														</p>
													</div>
													<div class="col-md-7 col-lg-4">
														<p class="value">
															<?php 
															$real = chk_array($modelo_processo_resumo->form_data, 'homorario_sucumbencias');
															echo   'R$	' . number_format($real, 2, ',', '.');?>
														</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="card card-table mb-4">
									<div class="card-header row">
										<div class="col-6">
											<h3 class="title text-left">
												Enviar feedback
											</h3>
										</div>
										<div class="col-6">
											<a href="<?php echo HOME_URI?>/plataforma/feedback/id/<?php echo chk_array($modelo_processo_resumo->form_data, 'idcliente'); ?>" class="pull-right">
												Exibir histórico
											</a>
										</div>
									</div>
									<div class="card-block">
										<div class="table table-responsive">
											<table class="table table-responsive no-border">
												<tbody>
													<tr>
														<td class="text-left text-nowrap">
															<a href="#">
																<img src="/assets/esad/temp/profile-leonardo.jpg" class="pull-left rounded-circle hidden-md-down" alt="Perfil">
																<div>
																	Felipe Lomeu
																	<small class="text-uppercase">
																		Underlab
																	</small>
																</div>
															</a>
														</td>
														<td class="hidden-sm-down">
															<a href="tel:+5527998876314">
																<small>
																	+55 27 99887-6314
																</small>
															</a>
														</td>
														<td class="hidden-md-down">
															<a href="tel:+552730242357">
																<small>
																	+55 27 3024-2357
																</small>
															</a>
														</td>
														<td class="hidden-md-down">
															<a href="email:lomeu@underlab.com.br">
																<small>
																	lomeu@underlab.com.br
																</small>
															</a>
														</td>
														<td>
															<a href="<?php echo HOME_URI?>/plataforma/feedback/id/<?php echo chk_array($modelo_processo_resumo->form_data, 'idcliente'); ?>" class="btn btn-rounded btn-info btn-sm pull-right">
																<i class="fa fa-envelope-o" aria-hidden="true"></i>
																Enviar Feedback
															</a>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="card card-table mb-4">
									<div class="card-header">
										<div class="row">
											<div class="col-6">
												<h3 class="title text-left">
													Compartilhamento
												</h3>
											</div>
											<div class="col-6">
												<button data-toggle="modal" data-target="#compartilhar" type="button" class="pull-right">
													Compartilhar
												</button>
												</a>
											</div>
										</div>
									</div>
									<div class="card-block">
										<div class="table">
											<table class="table table-striped table-hover">
												<thead>
													<tr>
														<th>Data</th>
														<th>Colaborador</th>
														<th>Último acesso</th>
														<th></th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>
															31/01/2017
														</td>
														<td>
															Felipe Lomeu
														</td>
														<td>
															10/01/2017 às 10h30
														</td>
														<td>
															<button type="button" class="text-danger" data-placement="top" title="Excluir">
																<i class="fa fa-trash-o"></i>
															</button>
														</td>
													</tr>
													<tr>
														<td>
															31/01/2017
														</td>
														<td>
															Felipe Lomeu
														</td>
														<td>
															10/01/2017 às 10h30
														</td>
														<td>
															<button type="button" class="text-danger" data-placement="top" title="Excluir">
																<i class="fa fa-trash-o"></i>
															</button>
														</td>
													</tr>
													<tr>
														<td>
															31/01/2017
														</td>
														<td>
															Felipe Lomeu
														</td>
														<td>
															10/01/2017 às 10h30
														</td>
														<td>
															<button type="button" class="text-danger" data-placement="top" title="Excluir">
																<i class="fa fa-trash-o"></i>
															</button>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</section>
						</div>
					</div>
        </div>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>

    <!--?php require ABSPATH . '/views/_includes/modal/_contato.php'; ?-->
    <?php require ABSPATH . '/views/_includes/modal/_share.php'; ?>
  </body>
</html>
