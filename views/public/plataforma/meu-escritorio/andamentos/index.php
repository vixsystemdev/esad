<?php
	 // General settings (relative path only here)
	 require ABSPATH . '/views/_includes/config.php';

	 $page = "meuescritorio";
?>

<!DOCTYPE html>
<html>
	<head>
		<?php require ABSPATH . '/views/_includes/metadata.php'; ?>
		<title>Andamentos | Meu Escritório | esad</title>
		<?php require ABSPATH . '/views/_includes/styles.php'; ?>
	</head>
	<body>
		<div class="wrapper">
			<div class="layout-navbar layout-fluid <!--?php echo $template; ?-->">
				<header>
					<?php require ABSPATH . "/views/_includes/navbar-fluid-plataforma.php"; ?>
				</header>
				<div class="wrapper-main">
					<div class="wrapper-content">
						<div class="container">
              <section>
                <div class="row">
                  <div class="col-lg-6">
      							<header class="page-header">
                      <h1>
                        Andamentos
                      </h1>
                      <p class="lead">
                        Aqui estão os andamentos recentes de todos os processos ativos cadastrados, caso queira separar os andamentos recentes por matéria, clique nas pastas abaixo.
                      </p>
                    </header>
                    					<form role="form" method="post" enctype="multipart/form-data" autocomplete="off">
                                          <div class="form-group">
                                            <div class="input-group">
                                              <input name="contato" id="processo" type="text" class="form-control" placeholder="Localizar processo">
                                              <span class="input-group-btn">
                                                <button class="btn btn-secondary" type="button" onclick="buscar()">
                                                  <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                              </span>
                                            </div>
                                          </div>
                   						</form>
                  </div>
                  <div class="col-lg-6">
                  </div>
                </div>
                <div class="row">
                  <div class="col-xl-3 col-lg-4 mb-4">
                    <ul class="nav flex-column nav-pills">
                     <?php $lista = $modelo_andamentos->listar_atividades();
						foreach($lista as $andamentos):?>
                          <li class="nav-item text-left">
                            <a class="nav-link" href="<?php echo HOME_URI; ?>/plataforma/andamentos/materia/<?php echo $andamentos['id']?>">
                              <i class="fa fa-folder" aria-hidden="true"></i>
                                <?php echo $andamentos['atividade']?>
                            </a>
                          </li>
                       <?php endforeach ?>
                    </ul>
                  </div>
                  <div class="col-xl-9 col-lg-8">
                    <div class="card card-table">
                      <div class="card-header">
                        <h3>
                          Andamentos recentes
                        </h3>
                      </div>
                      <div class="card-block">
                        <div class="table table-responsive">
                          <table class="table table-striped table-hover no-border">
                            <tbody>
								<?php $lista = $modelo_andamentos->lista_andamentos();
                                    foreach($lista as $andamentos):?>
                                       <tr class="andamento">
                                       <td>                                	
                                          <div class="title">
                                          
                                             <?php 
											 	$datamensagem = $andamentos['data'];
												$datamensagem = date("d/m/Y", strtotime($datamensagem));
												echo $datamensagem ?>
                                                
                                          </div>
                                          <div class="caption caption-andamento">
                                            Último andamento:
                                          </div>
                                          <div class="caption caption-localizacao">
                                            Localização
                                          </div>
                                          <div class="caption caption-materia">
                                            Matéria:
                                          </div>
                                          <div class="caption caption-fase">
                                            Fase:
                                          </div>
                                          <div class="caption caption-parte-ativa">
                                            Parte ativa:
                                          </div>
                                          <div class="caption caption-parte-passiva">
                                            Parte passiva:
                                          </div>
                                        </td>
                                        <td>
                                          <div class="title">
                                             <?php echo $andamentos['num_processo']?>
                                          </div>
                                          <div class="data data-andamento">
                                             <?php echo $andamentos['atividade']?>
                                          </div>
                                          <div class="data data-localizacao">
                                             <?php echo $andamentos['localizacao']?>
                                          </div>
                                          <div class="data data-materia">
                                             <?php echo $andamentos['materia']?>
                                          </div>
                                          <div class="data data-fase">
                                            Conhecimento
                                          </div>
                                          <div class="data data-parte-ativa">
                                             <?php echo $andamentos['parte_ativa']?>
                                          </div>
                                          <div class="data data-parte-passiva">
                                            <?php echo $andamentos['parte_passiva']?>
                                          </div>
                                        </td>
                                        <td>
                                          <div class="dropdown">
                                            <button class="text-default" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                              <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                              <a class="dropdown-item" href="<?php echo HOME_URI?>/plataforma/processos/processo-x/">
                                                Resumo
                                              </a>
                                              <a class="dropdown-item" href="<?php echo HOME_URI?>/plataforma/atividades/">
                                                Atividades
                                              </a>
                                              <a class="dropdown-item" href="<?php echo HOME_URI?>/plataforma/processo/">
                                                Arquivos
                                              </a>
                                              <a class="dropdown-item" href="<?php echo HOME_URI?>/plataforma/contatos/">
                                                Contatos
                                              </a>
                                              <a class="dropdown-item" href="<?php echo HOME_URI?>/plataforma/feedback/">
                                                Feedback para Cliente
                                              </a>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                   <?php endforeach ?>
                            </tbody>
                          </table>
                        </div>
                        <nav class="pagination" aria-label="table-pagination">
                          <div class="row">
                            <div class="col-6">
                              <small class="text-muted">
                                1 - 5 de 10
                              </small>
                            </div>
                            <div class="col-6">
                              <ul class="pagination-links float-right">
                                <li class="page-item">
                                  <a class="page-link" href="#">
                                    <i class="fa fa-chevron-left" aria-hidden="true"></i>
                                  </a>
                                </li>
                                <li class="page-item">
                                  <a class="page-link active" href="#">1</a>
                                </li>
                                <?php /*
                                <li class="page-item">
                                  <a class="page-link" href="#">2</a>
                                </li>
                                <li class="page-item">
                                  <a class="page-link" href="#">3</a>
                                </li>
                                */ ?>
                                <li class="page-item">
                                  <a class="page-link" href="#">
                                    <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </nav>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
						</div>
					</div>
        </div>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>
    
    <script>
    
		function buscar(){
					
			busca = $("#processo").val();
			window.location.href = "<?php echo HOME_URI?>/plataforma/andamentos/busca/" + busca;
					
		}
	
    </script>
  </body>
</html>
