<?php
	 // General settings (relative path only here)
	 require ABSPATH . '/views/_includes/config.php';

	 $page = "meuescritorio";
?>

<!DOCTYPE html>
<html>
	<head>
		<?php require ABSPATH . '/views/_includes/metadata.php'; ?>
		<title>Contatos | Meu Escritório | ESAD </title>
		<?php require ABSPATH . '/views/_includes/styles.php'; ?>
	</head>
	<body>
		<div class="wrapper">
			<div class="layout-navbar layout-fluid <?php //echo $template; ?>">
				<header>
					<?php require ABSPATH . "/views/_includes/navbar-fluid-plataforma.php"; ?>
				</header>
				<div class="wrapper-main">
					<div class="wrapper-content">
						<div class="container">
							<section>
								<div class="row">
									<div class="col-lg-6">
										<header class="page-header">
											<h1>
												Contatos
											</h1>
											<p class="lead">
												Aqui podem ser visualizados todos os seus contatos profissionais, podendo ser categorizados por clientes, sócios, advogados associados, estagiários, advogados parceiros e outros.
											</p>
										</header>
										<form role="form" method="post" enctype="multipart/form-data" autocomplete="off">
                                          <div class="form-group">
                                            <div class="input-group">
                                              <input name="contato" id="contato" type="text" class="form-control" placeholder="Localizar contato">
                                              <span class="input-group-btn">
                                                <button class="btn btn-secondary" type="button" onclick="buscar()">
                                                  <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                              </span>
                                            </div>
                                          </div>
                                        </form>
									</div>
									<div class="col-lg-6">
										<a href="<?php echo HOME_URI; ?>/plataforma/contatos/cadastro1" class="btn btn-inverse btn-rounded">
											<i class="fa fa-plus" aria-hidden="true"></i>
											Adicionar contato
										</a>
									</div>
								</div>
								<div class="card card-table">
									<div class="card-header">
										<div class="row">
											<div class="col-6">
												<h3 class="title text-left">
													Relação de contatos
												</h3>
											</div>
											<div class="col-6">
												<a href="<?php echo HOME_URI; ?>/plataforma/contatos/" class="pull-right">
													<small>
														Exibir apenas Contatos
													</small>
												</a>
											</div>
										</div>
									</div>
									<div class="card-block">
										<div class="table table-responsive">
											<table class="table table-striped table-hover">
												<tbody>
                                                    <?php $lista = $modelo_contatos->listar_contatos();
													  foreach($lista as $contatos):?>
													<tr>
														<td class="text-left text-nowrap">
															<a href="#">
																<img src="<?php echo HOME_URI; ?>/assets/esad/temp/profile-leonardo.jpg" class="pull-left rounded-circle hidden-md-down" alt="Perfil">
																<div>
																	<?php echo $contatos['nome']?>
																	<small class="text-uppercase">
																		<?php echo $contatos['empresa']?>
																	</small>
																</div>
															</a>
														</td>
														<td class="text-nowrap hidden-sm-down">
															<a href="tel:<?php echo $contatos['celular']?>">
																<small class="text-muted">
																	<?php echo $contatos['celular']?>
																</small>
															</a>
														</td>
														<td class="text-nowrap hidden-lg-down">
															<a href="tel:<?php echo $contatos['telefone']?>">
																<small class="text-muted">
																	<?php echo $contatos['telefone']?>
																</small>
															</a>
														</td>
														<td class="text-nowrap hidden-sm-down">
															<a href="mailto:<?php echo $contatos['email']?>">
																<small class="text-muted">
																	<?php echo $contatos['email']?>
																</small>
															</a>
														</td>
														<td>   
                                                        	<a href="<?php echo HOME_URI; ?>/plataforma/contatos/feed/<?php echo $contatos['id']?>" class="text-default"><i class="fa fa-envelope-o"></i> </a>                                                        
															<button data-toggle="modal" id="feedcontato" data-target="#enviar-feedback" type="button" class="text-default" data-placement="top" title="Enviar Feedback" style="display:none;">
																<i class="fa fa-envelope-o"></i>
															</button>
                                                            
                                                             <a href="<?php echo HOME_URI; ?>/plataforma/contatos/id/<?php echo $contatos['id']?>" class="text-default"> <i class="fa fa-user"></i> </a>
															<button data-toggle="modal" id="idcontato" data-target="#cadastro-contato" type="button" class="text-default" data-placement="top" title="Informações do Contato" style="display:none;">
																<i class="fa fa-user"></i>
															</button>
                                                            	<a href="<?php echo HOME_URI; ?>/plataforma/contatos/pro/<?php echo $contatos['id']?>" class="text-default"> <i class="fa fa-file-text-o"></i> </a>
															<button data-toggle="modal" id="procontato" data-target="#processos-contato" type="button" class="text-default" data-placement="top" title="Processos Associados" style="display:none;">
																<i class="fa fa-file-text-o"></i>
															</button>
                                                            	<a href="<?php echo HOME_URI; ?>/plataforma/contatos/del/<?php echo $contatos['id']?>" class="text-danger"> <i class="fa fa-trash-o"></i> </a>
																<button data-toggle="modal" id="delcontato" data-target="#excluir-contato" type="button" class="text-danger" data-placement="top" title="Excluir" style="display:none;">
																<i class="fa fa-trash-o"></i>
															</button>
														</td>
                                                        
													</tr>
                                                    <?php endforeach ?>
													

												</tbody>
											</table>
										</div>
										<nav class="pagination" aria-label="table-pagination">
											<div class="row">
												<div class="col-6">
													<small class="text-muted">
														1 - 5 de 10
													</small>
												</div>
												<div class="col-6">
													<ul class="pagination-links float-right">
														<li class="page-item">
														  <a class="page-link" href="#">
														    <i class="fa fa-chevron-left" aria-hidden="true"></i>
														  </a>
														</li>
														<li class="page-item">
														  <a class="page-link active" href="#">1</a>
														</li>
														<?php /*
														<li class="page-item">
														  <a class="page-link" href="#">2</a>
														</li>
														<li class="page-item">
														  <a class="page-link" href="#">3</a>
														</li>
														*/ ?>
														<li class="page-item">
														  <a class="page-link" href="#">
														    <i class="fa fa-chevron-right" aria-hidden="true"></i>
														  </a>
														</li>
													</ul>
												</div>
											</div>
										</nav>
									</div>
								</div>
							</section>
						</div>
					</div>
        </div>
      </div>
    </div>
    
   
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>
    <?php require ABSPATH . '/views/_includes/modal/_contato.php'; ?>
    <script>
    
        var x = window.location.toString().indexOf("feed") > 0;
            if (x == true){
                $( "#feedcontato" ).click();
        }
    
    	var x = window.location.toString().indexOf("id") > 0;
		if (x == true){
			$( "#idcontato" ).click();
		}
		
		var y = window.location.toString().indexOf("del") > 0;
		if (y == true){
			$( "#delcontato" ).click();
		}
        
        var y = window.location.toString().indexOf("pro") > 0;
		if (y == true){
			$( "#procontato" ).click();
		}	
		
		var y = window.location.toString().indexOf("cadastro1") > 0;
		if (y == true){
			$( "#cadastro-contato" ).modal('show');
		}
		
		var y = window.location.toString().indexOf("cadastro2") > 0;
		if (y == true){
			$( "#tipo-contato" ).modal('show');
		}
		
		var y = window.location.toString().indexOf("cadastro3") > 0;
		if (y == true){
			$( "#processos-contato" ).modal('show');
		}
		
		function buscar(){
					
			busca = $("#contato").val();
			window.location.href = "<?php echo HOME_URI?>/plataforma/contatos/busca/" + busca;
					
		}
    
    </script>
    
   
  </body>
</html>
