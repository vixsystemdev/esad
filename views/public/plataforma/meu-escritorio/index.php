<?php
	 // General settings (relative path only here)
	 require ABSPATH . '/views/_includes/config.php';

   $page = "meuescritorio";
?>

<!DOCTYPE html>
<html>
	<head>
		<?php require ABSPATH . '/views/_includes/metadata.php'; ?>
		<title>Dashboard | Meu Escritório | esad</title>
		<?php require ABSPATH . '/views/_includes/styles.php'; ?>
	</head>
	<body>
		<div class="wrapper">
			<div class="layout-navbar layout-fluid <?php //echo $template; ?>">
				<header>
					<?php require ABSPATH . '/views/_includes/navbar-fluid-plataforma.php'; ?>
				</header>
				<div class="wrapper-main">
					<div class="wrapper-content">
						<div class="container">
              <section>
  							<div class="row">
                  <div class="col-lg-6">
                    <header class="page-header">
                      <h1>
                        Principal
                      </h1>
                      <p class="lead">
                        Nesta tela você pode visualizar as informações gerais do seu esad. Caso deseje adicionar novo processo, clique no botão ao lado.
                      </p>
                    </header>
                  </div>
                  <div class="col-lg-6">
                    <a href="<?php echo HOME_URI?>/plataforma/novoprocesso/" class="btn btn-inverse btn-rounded">
                      <i class="fa fa-plus" aria-hidden="true"></i>
                      Adicionar processo
                    </a>
                  </div>
                </div>
                <?// Meus Processos ?>
                <div class="card mb-4 card-processos">
                  <div class="card-header row">
                    <h3 class="title text-left">
                      Meus Processos
                    </h3>
                  </div>
                  <div class="card-block">
                    <div class="heading">
                      <p>
                        Abaixo se encontra a quantidade de processos ativos,  <br class="hidden-xs-down">
                        divididos por área e por fase, e ainda, quantos já foram  <br class="hidden-xs-down">
                        cadastrados no total, somados ativos e arquivados.
                      </p>
                      <h4 class="value">
                        <span class="ativos-value">
                          40
                        </span>
                        <span class="ativos-label">
                          ativos
                        </span>
                        <span class="separator">
                          /
                        </span>
                        <span class="total-value">
                          75
                        </span>
                        <span class="total-label">
                          total
                        </span>
                      </h4>
                    </div>
                    <div class="row areas">
                      <div class="col-md-4">
                        <div class="area">
                          <h5>
                            Administrativo
                            <span class="pull-right">
                              02
                            </span>
                          </h5>
                        </div>
                        <div class="area">
                          <h5>
                            Civil
                            <span class="pull-right">
                              09
                            </span>
                          </h5>
                        </div>
                        <div class="area">
                          <h5>
                            Comercial
                            <span class="pull-right">
                              02
                            </span>
                          </h5>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="area">
                          <h5>
                            Do Consumidor
                            <span class="pull-right">
                              05
                            </span>
                          </h5>
                        </div>
                        <div class="area">
                          <h5>
                            Do Trabalho
                            <span class="pull-right">
                              01
                            </span>
                          </h5>
                        </div>
                        <div class="area">
                          <h5>
                            Família
                            <span class="pull-right">
                              06
                            </span>
                          </h5>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="area">
                          <h5>
                            Penal
                            <span class="pull-right">
                              15
                            </span>
                          </h5>
                        </div>
                        <div class="area">
                          <h5>
                            Tributário
                            <span class="pull-right">
                              21
                            </span>
                          </h5>
                        </div>
                        <div class="area">
                          <h5>
                            Outros
                            <span class="pull-right">
                              03
                            </span>
                          </h5>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?// Expectativa ?>
                </div>
                <?php // Fases ?>
                <div class="row mb-4">
                  <div class="col-md-4">
                    <div class="card card-fase card-fase-1">
                      <div class="card-block">
                        <h4>
                          Conhecimento
                        </h4>
                        <p>
                          10
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="card card-fase card-fase-2">
                      <div class="card-block">
                        <h4>
                          Recursal
                        </h4>
                        <p>
                          25
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="card card-fase card-fase-3">
                      <div class="card-block">
                        <h4>
                          Execução
                        </h4>
                        <p>
                          05
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
                <?// Meus Expectativas ?>
                <div class="card mb-4 card-expectativa">
                  <div class="card-header row">
                    <h3 class="title text-left">
                      Expectativa de ganho
                    </h3>
                  </div>
                  <div class="card-block">
                    <div class="row">
                      <div class="col-lg-6 heading">
                        <p>
                          Aqui o advogado pode visualizar qual a expectativa de ganho <br class="hidden-xs-down">
                          de todos os processos ativos cadastrados, divididos em <br class="hidden-xs-down">
                          honorários contratuais, de êxito e sucumbenciais.
                        </p>
                        <h4 class="value">
                          R$ 3.210,00
                        </h4>
                      </div>
                      <div class="col-lg-6 data">
                        <div class="row mt-3">
                          <div class="col-md-5 col-lg-8">
                            <p class="title">
                              Honorários contratuais:
                            </p>
                          </div>
                          <div class="col-md-7 col-lg-4">
                            <p class="value">
                              R$ 1.200,00
                            </p>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-5 col-lg-8">
                            <p class="title">
                              Percentual de êxito:
                            </p>
                          </div>
                          <div class="col-md-7 col-lg-4">
                            <p class="value">
                              R$ 2.220,00
                            </p>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-5 col-lg-8">
                            <p class="title">
                              Honorários sucumbenciais:
                            </p>
                          </div>
                          <div class="col-md-7 col-lg-4">
                            <p class="value">
                              R$ 0,00
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <?// Atividades ?>
                <div class="row mb-4">
                  <div class="col-lg-6 mb-xs-4 mb-sm-4 mb-md-4">
                    <div class="card card-table">
                      <div class="card-header">
                        <div class="row">
                          <div class="col-6">
                            <h3 class="title text-left">
                              Atividades processuais
                            </h3>
                          </div>
                          <div class="col-6">
                            <a href="<?php echo HOME_URI?>/plataforma/atividades/" class="pull-right">
                              Exibir todas
                            </a>
                          </div>
                        </div>
                      </div>
                      <div class="card-block">
                        <div class="table table-responsive">
                          <table class="table no-border table-hover">
                            <tbody>
                              <tr>
                                <td>
                                  <div class="checkbox">
                                    <label for="">
                                      <input type="checkbox" value="0">
                                      <span class="text-muted text-left pt-0">
                                        Diligência de terceiros
                                        <small>
                                          Loureiro & Diniz Sociedade de Advogados
                                        </small>
                                       </span>
                                    </label>
                                  </div>
                                </td>
                                <td>
                                  <span class="text-muted">
                                    <small>
                                      10/12/2017 ÀS 14h00
                                    </small>
                                  </span>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <div class="checkbox">
                                    <label for="">
                                      <input type="checkbox" value="0">
                                      <span class="text-muted text-left pt-0">
                                        Diligência de terceiros
                                        <small>
                                          Loureiro & Diniz Sociedade de Advogados
                                        </small>
                                       </span>
                                    </label>
                                  </div>
                                </td>
                                <td>
                                  <span class="text-muted">
                                    <small>
                                      10/12/2017 ÀS 14h00
                                    </small>
                                  </span>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <div class="checkbox">
                                    <label for="">
                                      <input type="checkbox" value="0">
                                      <span class="text-muted text-left pt-0">
                                        Diligência de terceiros
                                        <small>
                                          Loureiro & Diniz Sociedade de Advogados
                                        </small>
                                       </span>
                                    </label>
                                  </div>
                                </td>
                                <td>
                                  <span class="text-muted">
                                    <small>
                                      10/12/2017 ÀS 14h00
                                    </small>
                                  </span>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="card ">
                      <div class="card-header">
                        <h3 class="title text-left">
                          Calendário
                        </h3>
                      </div>
                      <div class="card-block">
                        
                      </div>
                    </div>
                  </div>
                </div>
                <?// Andamentos ?>
                <div class="card card-table">
                  <div class="card-header">
                    <div class="row">
                      <div class="col-6">
                        <h3 class="title text-left">
                          Andamentos recentes
                        </h3>
                      </div>
                      <div class="col-6">
                        <a href="<?php echo HOME_URI?>/plataforma/andamentos/" class="pull-right">
                          <small>
                            Exibir todos
                          </small>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div class="card-block">
                    <div class="table table-responsive">
                      <table class="table table-striped table-hover no-border">
                        <tbody>
                          <tr class="andamento">
                            <td>
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="row">
                                    <div class="col-lg-4">
                                      <div class="title">
                                        31/01/2017
                                      </div>
                                    </div>
                                    <div class="col-lg-8">
                                      <div class="title">
                                        0020508-82.2008.8.08.0035
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-lg-4">
                                      <div class="caption caption-andamento">
                                        Último andamento:
                                      </div>
                                    </div>
                                    <div class="col-lg-8">
                                      <div class="data data-andamento">
                                        Petição juntada aos autos
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-lg-4">
                                      <div class="caption caption-localizacao">
                                        Localização
                                      </div>
                                    </div>
                                    <div class="col-lg-8">
                                      <div class="data data-localizacao">
                                        Vila Velha - 4ª Vara Cível
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-lg-4">
                                      <div class="caption caption-materia">
                                        Matéria:
                                      </div>
                                    </div>
                                    <div class="col-lg-8">
                                      <div class="data data-materia">
                                        Cível
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-lg-4">
                                      <div class="caption caption-fase">
                                        Fase:
                                      </div>
                                    </div>
                                    <div class="col-lg-8">
                                      <div class="data data-fase">
                                        Conhecimento
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="row mt-5">
                                    <div class="col-lg-4">
                                      <div class="caption caption-parte-ativa">
                                        Parte ativa:
                                      </div>
                                    </div>
                                    <div class="col-lg-8">
                                      <div class="data data-parte-ativa">
                                        Felipe Jose da Rocha Lomeu
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-lg-4">
                                      <div class="caption caption-parte-passiva">
                                        Parte passiva:
                                      </div>
                                    </div>
                                    <div class="col-lg-8">
                                      <div class="data data-parte-passiva">
                                        Maria Celestina da Silva
                                        <br>
                                        Romeu Julieta Assis Fraga
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </td>
                            <td>
                              <div class="dropdown">
                                <button class="text-default" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                  <a class="dropdown-item" href="<?php echo HOME_URI?>/plataforma/processox/">
                                    Resumo
                                  </a>
                                  <a class="dropdown-item" href="<?php echo HOME_URI?>/plataforma/andamentos/">
                                    Andamentos
                                  </a>
                                  <a class="dropdown-item" href="<?php echo HOME_URI?>/plataforma/atividades/">
                                    Atividades
                                  </a>
                                  <a class="dropdown-item" href="<?php echo HOME_URI?>/plataforma/processo/">
                                    Arquivos
                                  </a>
                                  <a class="dropdown-item" href="<?php echo HOME_URI?>/plataforma/contatos/">
                                    Contatos
                                  </a>
                                  <a class="dropdown-item" href="<?php echo HOME_URI?>/plataforma/feedback/">
                                    Feedback para Cliente
                                  </a>
                                </div>
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </section>
						</div>
					</div>
        </div>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>
  </body>
</html>
