<?php
	 // General settings (relative path only here)
	 require ABSPATH . '/views/_includes/config.php';

	 $page = "meuescritorio";
?>

<!DOCTYPE html>
<html>
	<head>
		<?php require ABSPATH . '/views/_includes/metadata.php'; ?>
		<title>Arquivos | Meu Escritório | esad</title>
		<?php require ABSPATH . '/views/_includes/styles.php'; ?>
	</head>
	<body>
		<div class="wrapper">
			<div class="layout-navbar layout-fluid <!--?php echo $template; ?-->">
				<header>
					<?php require ABSPATH . "/views/_includes/navbar-fluid-plataforma.php"; ?>
				</header>
				<div class="wrapper-main">
					<div class="wrapper-content">
						<div class="container">
							<section>
								<div class="row">
									<div class="col-lg-6">
										<header class="page-header">
											<h1>
												Arquivos
											</h1>
											<p class="lead">
												Aqui estão todos os arquivos anexados recentemente. Caso queira visualizar arquivos de um processo específico, vá até a pasta do processo ou busque nos filtros abaixo.
											</p>
										</header>
										<form action="">
											<div class="form-group">
												<div class="input-group">
													<input name="processo" id="processo" type="text" class="form-control" placeholder="Localizar processo">
													<span class="input-group-btn">
										        <button class="btn btn-secondary" type="button">
										        	<i class="fa fa-search" aria-hidden="true"></i>
										        </button>
										      </span>
												</div>
											</div>
										</form>
									</div>
									<div class="col-lg-6">
										<button data-toggle="modal" data-target="#enviar-arquivos-select" class="btn btn-inverse btn-rounded" type="button">
											<i class="fa fa-cloud-upload" aria-hidden="true"></i>
											Anexar arquivo
										</button>
									</div>
								</div>
								<div class="row">
									<div class="col-xl-3 col-lg-4 mb-4">
										<?php include ABSPATH .'/views/_includes/_nav-arquivos.php'; ?>
                                        
									</div>
									<div class="col-xl-9 col-lg-8">
										<div class="card card-table">
											<div class="card-header">
												<h3 class="title text-left">
													Todos
												</h3>
											</div>
											<div class="card-block">
												<div class="table table-responsive">
													<table class="table no-border table-hover">
													<thead class="no-border">
														<tr>
															<th>
															</th>
															<th class="text-nowrap">
																Matéria <i class="fa fa-angle-down"></i>
															</th>
															<th class="text-center text-nowrap">
																Processos <i class="fa fa-angle-down"></i>
															</th>
															<th class="text-nowrap">
																Expectativa <i class="fa fa-angle-down"></i>
															</th>
															<th class="text-center text-nowrap">
																Último andamento <i class="fa fa-angle-down"></i>
															</th>
															<th class="text-center text-nowrap">
																Última alteração <i class="fa fa-angle-down"></i>
															</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>
																<i class="fa fa-folder fa-lg" aria-hidden="true"></i>
															</td>
															<td>
																<a href="<?php echo HOME_URI?>/plataforma/materia/">
																	Administrativo
																</a>
															</td>
															<td class="text-center">
																10
															</td>
															<td>
																R$ 201.013,00
															</td>
															<td class="text-center">
																10/05/2016
															</td>
															<td class="text-center">
																10/05/2016
															</td>
														</tr>
														<tr>
															<td>
																<i class="fa fa-folder fa-lg" aria-hidden="true"></i>
															</td>
															<td>
																<a href="<?php echo HOME_URI?>/plataforma/materia/">
																	Cível
																</a>
															</td>
															<td class="text-center">
																2
															</td>
															<td>
																R$ 31.000,00
															</td>
															<td class="text-center">
																21/06/2010
															</td>
															<td class="text-center">
																10/05/2016
															</td>
														</tr>
														<tr>
															<td>
																<i class="fa fa-folder fa-lg" aria-hidden="true"></i>
															</td>
															<td>
																<a href="<?php echo HOME_URI?>/plataforma/materia/">
																	Do Trabalho
																</a>
															</td>
															<td class="text-center">
																19
															</td>
															<td>
																R$ 130.391,00
															</td>
															<td class="text-center">
																20/04/2011
															</td>
															<td class="text-center">
																10/05/2016
															</td>
														</tr>
														<tr>
															<td>
																<div class="share">
																	<span class="fa-stack fa-lg">
															    	<i class="fa fa-folder fa-stack-2x" aria-hidden="true"></i>
															    	<i class="fa fa-user fa-inverse fa-stack-1x pull-right" aria-hidden="true"></i>
														    	</span>
														    </div>
															</td>
															<td>
																<a href="<?php echo HOME_URI?>/plataforma/materia/">
																	Família
																</a>
															</td>
															<td class="text-center">
																9
															</td>
															<td>
																R$ 105.391,00
															</td>
															<td class="text-center">
																20/04/2011
															</td>
															<td class="text-center">
																10/05/2016
															</td>
														</tr>
														<tr>
															<td>
																<i class="fa fa-folder fa-lg" aria-hidden="true"></i>
															</td>
															<td>
																<a href="<?php echo HOME_URI?>/plataforma/materia/">
																	Penal
																</a>
															</td>
															<td class="text-center">
																15
															</td>
															<td>
																R$ 91.019,00
															</td>
															<td class="text-center">
																20/04/2011
															</td>
															<td class="text-center">
																10/05/2016
															</td>
														</tr>
														<tr>
															<td>
																<div class="share">
																	<span class="fa-stack fa-lg">
															    	<i class="fa fa-folder fa-stack-2x" aria-hidden="true"></i>
															    	<i class="fa fa-user fa-inverse fa-stack-1x pull-right" aria-hidden="true"></i>
														    	</span>
														    </div>
															</td>
															<td>
																<a href="<?php echo HOME_URI?>/plataforma/materia/">
																	Tributário
																</a>
															</td>
															<td class="text-center">
																9
															</td>
															<td>
																R$ 83.103,00
															</td>
															<td class="text-center">
																20/04/2011
															</td>
															<td class="text-center">
																10/05/2016
															</td>
														</tr>
														<tr>
															<td>
																<div class="share">
																	<span class="fa-stack fa-lg">
															    	<i class="fa fa-folder fa-stack-2x" aria-hidden="true"></i>
															    	<i class="fa fa-user fa-inverse fa-stack-1x pull-right" aria-hidden="true"></i>
														    	</span>
														    </div>
															</td>
															<td>
																<a href="<?php echo HOME_URI?>/plataforma/materia/">
																	Outros
																</a>
															</td>
															<td class="text-center">
																19
															</td>
															<td>
																R$ 401.052,00
															</td>
															<td class="text-center">
																20/04/2011
															</td>
															<td class="text-center">
																10/05/2016
															</td>
														</tr>
													</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
					</div>
        </div>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>
    <?php require ABSPATH . '/views/_includes/modal/_arquivos.php'; ?>
  </body>
</html>
