<?php
	 	// General settings (relative path only here)
	 	require ABSPATH . '/views/_includes/config.php';

	 $page = "perfil";
?>

<!DOCTYPE html>
<html>
	<head>
		<?php require ABSPATH . '/views/_includes/metadata.php'; ?>
		<title>Meu Perfil | esad</title>
		<?php require ABSPATH . '/views/_includes/styles.php'; ?>
	</head>
	<body>
		<div class="wrapper">
			<div class="layout-navbar layout-fluid <?php echo $template; ?>">
				<header>
					<?php require ABSPATH . "/views/_includes/navbar-fluid-plataforma.php"; ?>
				</header>
				<div class="wrapper-main">
					<div class="wrapper-content">
						<div class="container">
							<section>
								<header class="page-header mb-8">
	                <h1>
	                  Meu Perfil
	                </h1>
	              </header>
								<form class="no-border" action="">
									<div class="my-5">
										<h2>
											Informações Principais
										</h2>
									</div>
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												<label for="name">
													Nome completo *
												</label>
												<input name="name" id="name" type="text" class="form-control form-lg" value="Leonardo Fraga">
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label for="email">
													Email *
												</label>
												<input name="email" id="email" type="email" class="form-control form-lg" value="leonardofraga@gmail.com">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												<label for="phone">
													Celular *
												</label>
												<input name="phone" id="phone" type="text" class="form-control form-lg" value="+55 27 99745-3073">
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label for="telefone">
													Telefone *
												</label>
												<input name="telefone" id="telefone" type="text" class="form-control form-lg" value="+55 27 3024-2357">
											</div>
										</div>
									</div>
									<hr>
									<div class="mt-9 mb-5">
										<h2>
											Informações Complementares
										</h2>
									</div>
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												<label for="cpf">
													CPF
												</label>
												<input name="cpf" id="cpf" type="text" class="form-control form-lg" value="320.130.490-04">
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label for="company">
													Nome do Escritório
												</label>
												<input name="company" id="company" type="text" class="form-control form-lg" value="Fraga & Associados">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												<label for="oab">
													OAB
												</label>
												<input name="oab" id="oab" type="text" class="form-control form-lg" value="30123">
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label for="oab-uf">OAB<small>/</small>UF</label>
		                    <select class="form-control custom-select" id="oab-uf">
		                      <option disabled="disabled">-</option>
		                      <option>Acre</option>
		                      <option>Alagoas</option>
		                      <option>Amapá</option>
		                      <option>Amazonas</option>
		                      <option>Bahia</option>
		                      <option>Ceará</option>
		                      <option>Distrito Federal</option>
		                      <option selected="selected" >Espírito Santo</option>
		                      <option>Goiás</option>
		                      <option>Maranhão</option>
		                      <option>Mato Grosso</option>
		                      <option>Mato Grosso do Sul</option>
		                      <option>Minas Gerais</option>
		                      <option>Pará</option>
		                      <option>Paraíba</option>
		                      <option>Paraná</option>
		                      <option>Pernambuco</option>
		                      <option>Piauí</option>
		                      <option>Rio de Janeiro</option>
		                      <option>Rio Grande do Norte</option>
		                      <option>Rio Grande do Sul</option>
		                      <option>Rondônia</option>
		                      <option>Roraima</option>
		                      <option>Santa Catarina</option>
		                      <option>São Paulo</option>
		                      <option>Sergipe</option>
		                      <option>Tocantins</option>
		                    </select>
											</div>
										</div>
									</div>
									<hr>
									<a href="#" class="btn btn-success btn-rounded btn-submit">
										<i class="fa fa-check" aria-hidden="true"></i> Salvar alterações
									</a>
								</form>
							</section>
						</div>
					</div>
        </div>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>
  </body>
</html>
