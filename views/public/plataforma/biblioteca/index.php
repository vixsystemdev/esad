<?php
	 // General settings (relative path only here)
	 require ABSPATH . '/views/_includes/config.php';

	 $page = "biblioteca";
?>

<!DOCTYPE html>
<html>
	<head>
		<?php require ABSPATH . '/views/_includes/metadata.php'; ?>
		<title>Biblioteca | esad</title>
		<?php require ABSPATH . '/views/_includes/styles.php'; ?>
	</head>
	<body>
		<div class="wrapper">
			<div class="layout-navbar layout-fluid <?php echo $template; ?>">
				<header>
					<?php require ABSPATH . "/views/_includes/navbar-fluid-plataforma.php"; ?>
				</header>
				<div class="wrapper-main">
					<div class="wrapper-content">
						<div class="container">
							<section>
								<header class="page-header">
									<h1>
										Biblioteca
									</h1>
									<p class="lead">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br class="hidden-xs-down">Eius id ratione pro.
									</p>
								</header>
								<div class="biblioteca">
									<div class="item">
										<div class="row">
											<div class="col-lg-9">
												<div class="cover">
													<img src="<?php echo HOME_URI?>/views/public/assets/esad/temp/livro.png" class="img-fluid" alt="Livro">
												</div>
												<div class="info">
													<h3 class="title">
														Direito Penal Esquematizado - Parte Especial - 7ª Ed. 2017 
													</h3>
													<p class="subtitle">
														Goncalves, Victor Eduardo Rios / (Coord.), Pedro Lenza
													</p>
													<p class="description">
														Esta obra aborda a Parte Especial do Código Penal por meio de linguagem clara e com projeto gráfico que facilita a compreensão, mostrando-se como ferramenta útil aos concursandos, estudantes e profissionais da área. Nesta 7ª edição, foram inseridos e comentados novos julgados do STF e do STJ sobre temas altamente relevantes, bem como introduzidas novas questões de concursos. Foram também inseridas súmulas e teses aprovadas pelo STJ quanto aos crimes de furto, violação de direitos autorais e descaminho. Foram comentadas as Leis n. 13.228/2015, 13.330/2016 e especial atenção foi dada à Lei n. 13.344/2016.
													</p>
												</div>
											</div>
											<div class="col-lg-3">
												<div class="buy">
													<p>
														<small>
															De: R$ 145,00
														</small>
														por R$ 120,00
													</p>
													<button class="btn btn-success btn-rounded btn-submit">
					                	<i class="fa fa-shopping-cart" aria-hidden="true"></i> Comprar
					                </button>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="row">
											<div class="col-lg-9">
												<div class="cover">
													<img src="<?php echo HOME_URI?>/views/public/assets/esad/temp/livro.png" class="img-fluid" alt="Livro">
												</div>
												<div class="info">
													<h3 class="title">
														Direito Penal Esquematizado - Parte Especial - 7ª Ed. 2017 
													</h3>
													<p class="subtitle">
														Goncalves, Victor Eduardo Rios / (Coord.), Pedro Lenza
													</p>
													<p class="description">
														Esta obra aborda a Parte Especial do Código Penal por meio de linguagem clara e com projeto gráfico que facilita a compreensão, mostrando-se como ferramenta útil aos concursandos, estudantes e profissionais da área. Nesta 7ª edição, foram inseridos e comentados novos julgados do STF e do STJ sobre temas altamente relevantes, bem como introduzidas novas questões de concursos. Foram também inseridas súmulas e teses aprovadas pelo STJ quanto aos crimes de furto, violação de direitos autorais e descaminho. Foram comentadas as Leis n. 13.228/2015, 13.330/2016 e especial atenção foi dada à Lei n. 13.344/2016.
													</p>
												</div>
											</div>
											<div class="col-lg-3">
												<div class="buy">
													<p>
														<small>
															De: R$ 145,00
														</small>
														por R$ 120,00
													</p>
													<button class="btn btn-success btn-rounded btn-submit">
					                	<i class="fa fa-shopping-cart" aria-hidden="true"></i> Comprar
					                </button>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="row">
											<div class="col-lg-9">
												<div class="cover">
													<img src="<?php echo HOME_URI?>/views/public/assets/esad/temp/livro.png" class="img-fluid" alt="Livro">
												</div>
												<div class="info">
													<h3 class="title">
														Direito Penal Esquematizado - Parte Especial - 7ª Ed. 2017 
													</h3>
													<p class="subtitle">
														Goncalves, Victor Eduardo Rios / (Coord.), Pedro Lenza
													</p>
													<p class="description">
														Esta obra aborda a Parte Especial do Código Penal por meio de linguagem clara e com projeto gráfico que facilita a compreensão, mostrando-se como ferramenta útil aos concursandos, estudantes e profissionais da área. Nesta 7ª edição, foram inseridos e comentados novos julgados do STF e do STJ sobre temas altamente relevantes, bem como introduzidas novas questões de concursos. Foram também inseridas súmulas e teses aprovadas pelo STJ quanto aos crimes de furto, violação de direitos autorais e descaminho. Foram comentadas as Leis n. 13.228/2015, 13.330/2016 e especial atenção foi dada à Lei n. 13.344/2016.
													</p>
												</div>
											</div>
											<div class="col-lg-3">
												<div class="buy">
													<p>
														<small>
															De: R$ 145,00
														</small>
														por R$ 120,00
													</p>
													<button class="btn btn-success btn-rounded btn-submit">
					                	<i class="fa fa-shopping-cart" aria-hidden="true"></i> Comprar
					                </button>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="row">
											<div class="col-lg-9">
												<div class="cover">
													<img src="<?php echo HOME_URI?>/views/public/assets/esad/temp/livro.png" class="img-fluid" alt="Livro">
												</div>
												<div class="info">
													<h3 class="title">
														Direito Penal Esquematizado - Parte Especial - 7ª Ed. 2017 
													</h3>
													<p class="subtitle">
														Goncalves, Victor Eduardo Rios / (Coord.), Pedro Lenza
													</p>
													<p class="description">
														Esta obra aborda a Parte Especial do Código Penal por meio de linguagem clara e com projeto gráfico que facilita a compreensão, mostrando-se como ferramenta útil aos concursandos, estudantes e profissionais da área. Nesta 7ª edição, foram inseridos e comentados novos julgados do STF e do STJ sobre temas altamente relevantes, bem como introduzidas novas questões de concursos. Foram também inseridas súmulas e teses aprovadas pelo STJ quanto aos crimes de furto, violação de direitos autorais e descaminho. Foram comentadas as Leis n. 13.228/2015, 13.330/2016 e especial atenção foi dada à Lei n. 13.344/2016.
													</p>
												</div>
											</div>
											<div class="col-lg-3">
												<div class="buy">
													<p>
														<small>
															De: R$ 145,00
														</small>
														por R$ 120,00
													</p>
													<button class="btn btn-success btn-rounded btn-submit">
					                	<i class="fa fa-shopping-cart" aria-hidden="true"></i> Comprar
					                </button>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="row">
											<div class="col-lg-9">
												<div class="cover">
													<img src="<?php echo HOME_URI?>/views/public/assets/esad/temp/livro.png" class="img-fluid" alt="Livro">
												</div>
												<div class="info">
													<h3 class="title">
														Direito Penal Esquematizado - Parte Especial - 7ª Ed. 2017 
													</h3>
													<p class="subtitle">
														Goncalves, Victor Eduardo Rios / (Coord.), Pedro Lenza
													</p>
													<p class="description">
														Esta obra aborda a Parte Especial do Código Penal por meio de linguagem clara e com projeto gráfico que facilita a compreensão, mostrando-se como ferramenta útil aos concursandos, estudantes e profissionais da área. Nesta 7ª edição, foram inseridos e comentados novos julgados do STF e do STJ sobre temas altamente relevantes, bem como introduzidas novas questões de concursos. Foram também inseridas súmulas e teses aprovadas pelo STJ quanto aos crimes de furto, violação de direitos autorais e descaminho. Foram comentadas as Leis n. 13.228/2015, 13.330/2016 e especial atenção foi dada à Lei n. 13.344/2016.
													</p>
												</div>
											</div>
											<div class="col-lg-3">
												<div class="buy">
													<p>
														<small>
															De: R$ 145,00
														</small>
														por R$ 120,00
													</p>
													<button class="btn btn-success btn-rounded btn-submit">
					                	<i class="fa fa-shopping-cart" aria-hidden="true"></i> Comprar
					                </button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
					</div>
        </div>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>
  </body>
</html>
