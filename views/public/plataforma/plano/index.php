<?php
	 	// General settings (relative path only here)
	 	require ABSPATH . '/views/_includes/config.php';

	 $page 		= "plano";
	 $plano 	= "Premium"
?>

<!DOCTYPE html>
<html>
	<head>
		<?php require ABSPATH . '/views/_includes/metadata.php'; ?>
		<title>Plano | ESAD</title>
		<?php require ABSPATH . '/views/_includes/styles.php'; ?>
	</head>
	<body>
		<div class="wrapper">
			<div class="layout-navbar layout-fluid <!--?php echo $template; ?-->">
				<header>
					<?php require ABSPATH . "/views/_includes/navbar-fluid-plataforma.php"; ?>
				</header>
				<div class="wrapper-main">
					<div class="wrapper-content">
						<div class="container">
							<section>
								<header class="page-header">
	                <h1>
	                  Meu Plano
	                </h1>
	                <p class="lead">
	                	Escolha abaixo o plano que mais se adequa à sua realidade, lembrando que o esad foi criado para atender as necessidades do dia a dia dos Advogados, resolvendo problemas de forma simples.
	                </p>
	              </header>
								<div class="my-5">
									<h2>
										Informações sobre seu Plano
									</h2>
									<p>
										Este plano é indicado para advogados que tem uma certa quantidade de processos a serem gerenciados (entre 4 e 25 processos) ou que querem utilizar todas as funcionalidades oferecidas pelo esad.
									</p>
								</div>
								<div class="pricing">
									<div class="container">
										<div class="card current-plan card-collapse">
			            					<div class="card-block">
												<h3 class="title">
													Seu Plano: <?php echo $plano; ?>
												</h3>
												<p>
													Sua assinatura expira em 10/12/2017
												</p>
											</div>
										</div>
									</div>
								</div>
		          				<?php require ABSPATH . '/views/_includes/_pricing.php'; ?>
							</section>
							<hr>
							<section>
								<div class="mt-9 mb-6">
									<h2>
										Histórico de Pagamentos
									</h2>
									<p>
										Abaixo estão todos os pagamentos que você já fez, oferecendo ao Advogado o total controle de seus gastos com o esad.
									</p>
								</div>
								<div class="card card-table">
									<div class="card-block">
										<div class="table table-responsive">
											<table class="table table-striped table-hover">
												<thead>
													<tr>
														<th>Data</th>
														<th>Transação</th>
														<th class="hidden-xs-down">Descrição</th>
														<th>Valor</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>
															31/05/2017
														</td>
														<td>
															<a href="#">
																FWDZ9NPLTHS6
															</a>
														</td>
														<td class="hidden-xs-down">
															Plano Premium (com 10 processos)
														</td>
														<td>
															R$ 39,90
														</td>
													</tr>
													<tr>
														<td>
															31/04/2017
														</td>
														<td>
															<a href="#">
																ZG4HD88CQ52D
															</a>
														</td>
														<td class="hidden-xs-down">
															Plano Premium (com 10 processos)
														</td>
														<td>
															R$ 39,90
														</td>
													</tr>
													<tr>
														<td>
															31/03/2017
														</td>
														<td>
															<a href="#">
																P613G2XPC5K5
															</a>
														</td>
														<td class="hidden-xs-down">
															Plano Premium (com 9 processos)
														</td>
														<td>
															R$ 36,90
														</td>
													</tr>
													<tr>
														<td>
															31/02/2017
														</td>
														<td>
															<a href="#">
																CTQN2TQMK1FX
															</a>
														</td>
														<td class="hidden-xs-down">
															Plano Premium (com 9 processos)
														</td>
														<td>
															R$ 36,90
														</td>
													</tr>
													<tr>
														<td>
															31/01/2017
														</td>
														<td>
															<a href="#">
																FWDZ9NPLTHS6
															</a>
														</td>
														<td class="hidden-xs-down">
															Plano Premium (com 2 processos)
														</td>
														<td>
															R$ 15,90
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<nav class="pagination" aria-label="table-pagination">
											<div class="row">
												<div class="col-6">
													<small class="text-muted">
														1 - 5 de 10
													</small>
												</div>
												<div class="col-6">
													<ul class="pagination-links float-right">
														<li class="page-item">
														  <a class="page-link" href="#">
														    <i class="fa fa-chevron-left" aria-hidden="true"></i>
														  </a>
														</li>
														<li class="page-item">
														  <a class="page-link active" href="#">1</a>
														</li>
														<?php /*
														<li class="page-item">
														  <a class="page-link" href="#">2</a>
														</li>
														<li class="page-item">
														  <a class="page-link" href="#">3</a>
														</li>
														*/ ?>
														<li class="page-item">
														  <a class="page-link" href="#">
														    <i class="fa fa-chevron-right" aria-hidden="true"></i>
														  </a>
														</li>
													</ul>
												</div>
											</div>
										</nav>
									</div>
								</div>
							</section>
						</div>
					</div>
        </div>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>
  </body>
</html>
