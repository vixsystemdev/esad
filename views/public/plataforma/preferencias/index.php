<?php
	 	// General settings (relative path only here)
	 	require ABSPATH . '/views/_includes/config.php';

	 $page = "preferencias";
?>

<!DOCTYPE html>
<html>
	<head>
		<?php require ABSPATH . '/views/_includes/metadata.php'; ?>
		<title>Preferências | esad</title>
		<?php require ABSPATH . '/views/_includes/styles.php'; ?>
	</head>
	<body>
		<div class="wrapper">
			<div class="layout-navbar layout-fluid <?php echo $template; ?>">
				<header>
					<?php require ABSPATH . "/views/_includes/navbar-fluid-plataforma.php"; ?>
				</header>
				<div class="wrapper-main">
					<div class="wrapper-content">
						<div class="container">
							<section>
								<header class="page-header">
	                <h1>
	                  Preferências
	                </h1>
	                <p class="lead">
                    Neste menu você pode configurar o esad de maneira <br class="hidden-xs-down">que ele se adeque à sua rotina.
                  </p>
	              </header>
	              <form class="no-border" action="">
                  <div class="my-5">
                    <h2>
                      Segurança
                    </h2>
                    <p>
                      Para maior segurança, sua senha deve conter no mínimo 8 caracteres, podendo ser letras maiúsculas ou minúsculas e números.
                    </p>
                  </div>
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label for="name">
                          Senha atual *
                        </label>
                        <input name="password" id="password" type="password" class="form-control form-lg">
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label for="name">
                          Nova senha *
                        </label>
                        <input name="newPass" id="newPass" type="password" class="form-control form-lg">
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label for="name">
                          Confirmar nova senha *
                        </label>
                        <input name="newPassConfirm" id="newPassConfirm" type="password" class="form-control form-lg">
                      </div>
                    </div>
                  </div>
                  <hr>
                  <div class="mt-9 mb-5">
                    <h2>
                      Compartilhamento de Matérias
                    </h2>
                    <p>
                      O esad é um sistema em que você pode se comunicar não só com clientes, pelo feedback, mas também com outros advogados, abaixo você poderá configurar o compartilhamento das suas informações.
                    </p>
                  </div>
                  <?// Meus Processos ?>
                  <div class="card mb-4 card-processos">
                    <div class="card-header">
                      <h3 class="text-left title">
                        Compartilhar por área de atuação
                      </h3>
                    </div>
                    <div class="card-block">
                      <div class="row areas">
                        <div class="col-md-4">
                          <div class="area">
                            <button data-toggle="modal" data-target="#compartilhar" type="button">
                              <h5>
                                Administrativo
                              </h5>
                             </button>
                          </div>
                          <div class="area">
                            <button data-toggle="modal" data-target="#compartilhar" type="button">
                              <h5>
                                Civil
                              </h5>
                             </button>
                          </div>
                          <div class="area">
                            <button data-toggle="modal" data-target="#compartilhar" type="button">
                              <h5>
                                Comercial
                              </h5>
                             </button>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="area">
                            <button data-toggle="modal" data-target="#compartilhar" type="button">
                              <h5>
                                Do Consumidor
                              </h5>
                             </button>
                          </div>
                          <div class="area">
                            <button data-toggle="modal" data-target="#compartilhar" type="button">
                              <h5>
                                Do Trabalho
                              </h5>
                             </button>
                          </div>
                          <div class="area">
                            <button data-toggle="modal" data-target="#compartilhar" type="button">
                              <h5>
                                Família
                              </h5>
                             </button>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="area">
                            <button data-toggle="modal" data-target="#compartilhar" type="button">
                              <h5>
                                Penal
                              </h5>
                             </button>
                          </div>
                          <div class="area">
                            <button data-toggle="modal" data-target="#compartilhar" type="button">
                              <h5>
                                Tributário
                              </h5>
                             </button>
                          </div>
                          <div class="area">
                            <button data-toggle="modal" data-target="#compartilhar" type="button">
                              <h5>
                                Outros
                              </h5>
                             </button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?// Expectativa ?>
                  </div>
                  <button data-toggle="modal" data-target="#compartilhar" type="button" class="btn btn-inverse btn-sm btn-rounded mb-4">
                     <i class="fa fa-share-alt" aria-hidden="true"></i> Compartilhar todo meu Esad
                  </button>
                  <hr>
                  <div class="mt-9 mb-5">
                    <h2>
                      Comunicação
                    </h2>
                    <p>
                      O esad se comunica com seus usuários rotineiramente, <br class="hidden-xs-down">abaixo escolha quais as formas de sua preferência:
                    </p>
                  </div>
                  <div class="form-group d-inline-block">
                    <div class="checkbox form-control square">
                      <label for="">
                        <input type="checkbox" name="custom_checkbox square" value="1">
                        <span>
                          Notificações por e-mail
                        </span>
                      </label>
                    </div>
                    <div class="checkbox form-control square">
                      <label for="">
                        <input type="checkbox" name="custom_checkbox square" value="1">
                        <span>
                          Notificação pelo app (disponível para iOS e Android).
                        </span>
                      </label>
                    </div>
                  </div>
                  <hr>
                  <a href="#" class="btn btn-success btn-rounded btn-submit">
                    Salvar alterações
                  </a>
                </form>
							</section>
						</div>
					</div>
        </div>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>
    <?php require ABSPATH . '/views/_includes/modal/_share.php'; ?>
  </body>
</html>
