<?php
  // General settings (relative path only here)
  require ABSPATH . '/views/_includes/config.php';
?>

<!DOCTYPE html>
<html>

  <head>
    <?php require ABSPATH . '/views/_includes/metadata.php'; ?>
    <title>Esqueci minha senha | esad</title>
    <?php require ABSPATH . '/views/_includes/styles.php'; ?>
  </head>

  <body>
    <div class="wrapper">
      <div class="auth login">
        <header>
          <a class="brand" href="/">
            <img class="brand" src="/assets/esad/img/logo-esad.png" alt="esad">
          </a>
        </header>
        <section>
          <div class="card">
            <div class="card-header">
              <div class="background-line">
                <span>
                  Esqueci minha senha
                </span>
              </div>
            </div>
            <div class="card-block">
              <p class="card-text">
                Caso tenha esquecido sua senha, informe no campo abaixo o e-mail utilizado no cadastro. Em seguida, você receberá neste e-mail instruções para redefinir sua senha.
              </p>
              <form action="esqueci-minha-senha-retorno.php" role="form">
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="fa fa-envelope"></i>
                    </span>
                    <input id="email" type="text" class="form-control form-lg" name="email" placeholder="seu email">
                  </div>
                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block">
                  Continuar
                </button>
              </form>
            </div>
            <div class="card-footer">
              <a href="#">
                Precisa de ajuda? Fale com nossa equipe.
              </a>
            </div>
          </div>
        </section>
        <div class="cta">
          <a href="/cadastro/">
            <small>
              Não é cadastrado?
            </small>
            Cadastre-se agora!
          </a>
        </div>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>
  </body>

</html>
