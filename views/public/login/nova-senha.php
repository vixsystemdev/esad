<?php
	// General settings (relative path only here)
	require ABSPATH . '/views/_includes/config.php';
?>

<!DOCTYPE html>
<html>

  <head>
    <?php require ABSPATH . '/views/_includes/metadata.php'; ?>
    <title>Nova senha | esad</title>
    <?php require ABSPATH . '/views/_includes/styles.php'; ?>
  </head>

  <body>
    <div class="wrapper">
      <div class="auth login">
        <header>
          <a class="brand" href="/">
            <img class="brand" src="/assets/esad/img/logo-esad.png" alt="esad">
          </a>
        </header>
        <section>
            <div class="card">
              <div class="card-header">
                <div class="background-line">
                  <span>
                    Definição de senha
                  </span>
                </div>
              </div>
              <div class="card-block">
                <p class="card-text card-text-sm pt-2 pb-1">
                  Para definir sua nova senha, utilize pelo menos 8 caracteres.
                </p>
                <p class="card-text card-text-sm pb-3">
                  Aviso de segurança: Evite o uso de senhas que já tenham sido utilizadas em outros sites ou que sejam muito óbvias, por exemplo, nomes ou datas significativas para você ou para alguém próximo.
                </p>
                <form action="nova-senha-retorno.php" role="form">
                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="fa fa-lock"></i>
                      </span>
                      <input id="new" type="password" class="form-control form-lg" name="newPass" placeholder="nova senha">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="fa fa-lock"></i>
                      </span>
                      <input id="confirm" type="password" class="form-control form-lg" name="confirmPass" placeholder="repetir nova senha">
                    </div>
                  </div>
                  <button type="submit" class="btn btn-primary btn-lg btn-block">
                    Confirmar nova senha
                  </button>
                </form>
              </div>
              <div class="card-footer">
                <a href="#">
                  Precisa de ajuda? Fale com nossa equipe.
                </a>
              </div>
            </div>
        </section>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>
  </body>

</html>
