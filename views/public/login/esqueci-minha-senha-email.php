<?php
  // General settings (relative path only here)
  require ABSPATH . '/views/_includes/config.php';
?>

<!DOCTYPE html>
<html>

  <head>
    <?php require ABSPATH . '/views/_includes/metadata.php'; ?>
    <title>Esqueci minha senha | esad</title>
    <?php require ABSPATH . '/views/_includes/styles.php'; ?>
  </head>

  <body>
    <div class="wrapper">
      <div class="auth login">
        <header>
          <a class="brand" href="/">
            <img class="brand" src="/assets/esad/img/logo-esad.png" alt="esad">
          </a>
        </header>
        <section>
          <div class="card">
            <div class="card-header">
              <h3>
                Redefinição de Senha
              </h3>
              <span>
                Confirme sua solicitação
              </span>
            </div>
            <div class="card-block mt-4">
              <p class="card-text text-left">
                Foi iniciado o procedimento de redefinição de senha do seu acesso. Para confirmar esta solicitação e criar uma nova senha, por favor clique no link a seguir:
              </p>
              <div class="py-4">
                <button type="button" onclick="window.location.href='nova-senha.php'" class="btn btn-primary btn-lg btn-block">
                  Redefinir senha
                </button>
                <p class="mt-3">
                  <small>
                    Se preferir, clique no link abaixo <br>ou copie e cole em seu navegador:
                     <a href="nova-senha.php">
                       http://esad.com.br/forgot/50/cf3258e6befd16f07c
                     </a>
                  </small>
                </p>
              </div>
              <p class="card-text text-left">
                Se você não fez esta solicitação, ignore esta mensagem. Sua senha permanecerá inalterada.
              </p>
              <p class="card-text text-left">
                Tenha um ótimo dia!
                <br>
                Equipe esad
              </p>
            </div>
          </div>
        </section>
        </div>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>
  </body>

</html>
