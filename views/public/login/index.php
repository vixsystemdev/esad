<?php
  // General settings (relative path only here)
 // require ABSPATH . '/views/_includes/config.php';
  require ABSPATH . '/views/_includes/config.php';
?>

<!DOCTYPE html>
<html>

	<head>
		<?php require ABSPATH . '/views/_includes/metadata.php'; ?>
		<title>Entrar | esad</title>
        <?php require ABSPATH . '/views/_includes/styles.php'; ?>

	</head>
	<body>
		<div class="wrapper">
      <div class="auth login">
        <header>
          <a class="brand" href="/">
            <img class="brand" src="<?php echo HOME_URI?>/assets/esad/img/logo-esad.png" alt="esad">
          </a>
        </header>
        <section>
          <div class="card">
            <div class="card-block" >
               <button type="button" class="btn btn-facebook btn-lg btn-block" onclick="location.href='<?php echo HOME_URI?>/plataforma/'">
                <i class="fa fa-facebook-official" aria-hidden="true"></i> Entrar com Facebook
              </button>
              <div class="background-line">
                <span>
                  ou
                </span>
              </div>
              <form method="post">
                <div class="alert alert-danger pt-0 pb-3" hidden role="alert">
                  Mensagem de erro
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="fa fa-envelope fa-lg"></i>
                    </span>
                  
                    <input id="text" type="text" class="form-control form-lg" name="userdata[login]" placeholder="Login">
                  </div>
                </div>
                <div class="form-group mb-2">
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="fa fa-lock fa-lg"></i>
                    </span>
                    <input id="password" type="password" class="form-control form-lg" name="userdata[senha]" placeholder="Senha">
                  </div>
                </div>
                <div class="form-check form-check-sm mt-3">
                  <label class="form-check-label">
                    <input class="form-check-input" type="checkbox">
                      Continuar conectado
                  </label>
                </div>
                <button type="submit" class="btn btn-info btn-lg btn-block">
                  Entrar
                </button>
              </form>
            </div>
            
            <?php
			if ( $this->login_error ) {
				echo '<span style="color:red;">' . $this->login_error . '</span>' ;
			}
			?>
            
            <div class="card-footer">
              <a href="esqueci-minha-senha.php">
                  Esqueceu sua senha?
              </a>
            </div>
          </div>
        </section>
        <div class="cta">
          <a href="/cadastro/">
            <small>
              Não é cadastrado?
            </small>
            Cadastre-se agora!
          </a>
        </div>
      </div>
    </div>
		<?php 
		require ABSPATH . '/views/_includes/scripts.php';
		//include "_includes/scripts.php"; ?>
	</body>
</html>