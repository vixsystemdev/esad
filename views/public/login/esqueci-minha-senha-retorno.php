<?php
  // General settings (relative path only here)
  require ABSPATH . '/views/_includes/config.php';
?>

<!DOCTYPE html>
<html>

  <head>
    <?php require ABSPATH . '/views/_includes/metadata.php'; ?>
    <title>Esqueci minha senha | esad</title>
    <?php require ABSPATH . '/views/_includes/styles.php'; ?>
  </head>

  <body>
    <div class="wrapper">
      <div class="auth login">
        <header>
          <a class="brand" href="/">
            <img class="brand" src="/assets/esad/img/logo-esad.png" alt="esad">
          </a>
        </header>
        <section>
          <div class="card">
            <div class="card-header">
              <div class="background-line">
                <span>
                  Redefinição de senha
                </span>
              </div>
            </div>
            <div class="card-block">
              <p class="card-title pt-2 pb-8">
                <a href="esqueci-minha-senha-email.php">
	                Enviamos para o e-mail
	                <strong class="d-block">
	                  leonardofr***@gmail.com
	                </strong>
	                instruções para a redefinição da sua senha.
	              </a>
              </p>
            </div>
            <div class="card-footer">
              <a href="#">
                Precisa de ajuda? Fale com nossa equipe.
              </a>
            </div>
          </div>
        </section>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>
  </body>

</html>
