<?php
	// General settings (relative path only here)
  require ABSPATH . '/views/_includes/config.php';
?>

<!DOCTYPE html>
<html>

  <head>
    <?php require ABSPATH . '/views/_includes/metadata.php'; ?>
    <title>New password | Step 2 | Carbon</title>
    <?php require ABSPATH . '/views/_includes/styles.php'; ?>
  </head>

  <body>
    <div class="wrapper">
      <div class="auth login">
        <header>
          <a class="brand" href="/">
            <img class="brand" src="/assets/esad/img/logo-esad.png" alt="esad">
          </a>
        </header>
        <section>
          <div class="card">
            <div class="card-header">
              <div class="background-line">
                <span>
                  Definição de senha
                </span>
              </div>
            </div>
            <div class="card-block">
              <p class="card-title pt-4 pb-7">
              	Sua nova senha <br class="hidden-sm-down">foi definida com sucesso!
              </p>
              <a class="btn btn-primary btn-lg btn-block" href="/login/">
                Ok
              </a>
            </div>
            <div class="card-footer">
              <a href="#">
                Precisa de ajuda? Fale com nossa equipe.
              </a>
            </div>
          </div>
        </section>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>
  </body>

</html>
