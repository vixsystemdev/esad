<?php
// General settings (relative path only here)
	require ABSPATH . '/views/_includes/config.php';
?>

<!DOCTYPE html>
<html>

  <head>
    <?php require ABSPATH . '/views/_includes/metadata.php'; ?>
    <title>Concluir Cadastro | esad</title>
    <?php require ABSPATH . '/views/_includes/styles.php'; ?>
    <link rel="stylesheet" href="/components/pages/register/register.css">
  </head>

  <body>
    <div class="wrapper">
      <div class="auth register">
        <header>
          <a class="brand" href="/">
            <img class="brand" src="/assets/esad/img/logo-esad.png" alt="esad">
          </a>
        </header>
        <section>
          <header class="mb-4">
            <h1 class="h2 font-weight-normal">
              Informações complementares
            </h1>
          </header>
          <div class="card cardy-py-sm card-lg">
            <div class="card-block">
              <a class="float-right" href="planos.php">
                Pular etapa
                <i class="fa fa-lg fa-angle-right"></i>
              </a>
              <form class="form" role="form" action="planos.php" method="post">
                <div class="form-group">
                  <label for="celular">Telefone celular:</label>
                  <input id="celular" type="text" class="form-control form-lg" name="celular">
                </div>
                <div class="form-group">
                  <label for="telefone">Telefone comercial:</label>
                  <input id="telefone" type="text" class="form-control form-lg" name="telefone">
                </div>
                <div class="form-group">
                  <label for="cpf">CPF:</label>
                  <input id="cpf" type="text" class="form-control form-lg" name="cpf">
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="oab">OAB:</label>
                      <input id="oab" type="text" class="form-control form-lg" name="oab">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="oab-uf">OAB<small>/</small>UF</label>
                      <select class="form-control custom-select" id="oab-uf">
                        <option selected="selected" disabled="disabled">-</option>
                        <option>Acre</option>
                        <option>Alagoas</option>
                        <option>Amapá</option>
                        <option>Amazonas</option>
                        <option>Bahia</option>
                        <option>Ceará</option>
                        <option>Distrito Federal</option>
                        <option>Espírito Santo</option>
                        <option>Goiás</option>
                        <option>Maranhão</option>
                        <option>Mato Grosso</option>
                        <option>Mato Grosso do Sul</option>
                        <option>Minas Gerais</option>
                        <option>Pará</option>
                        <option>Paraíba</option>
                        <option>Paraná</option>
                        <option>Pernambuco</option>
                        <option>Piauí</option>
                        <option>Rio de Janeiro</option>
                        <option>Rio Grande do Norte</option>
                        <option>Rio Grande do Sul</option>
                        <option>Rondônia</option>
                        <option>Roraima</option>
                        <option>Santa Catarina</option>
                        <option>São Paulo</option>
                        <option>Sergipe</option>
                        <option>Tocantins</option>
                      </select>
                    </div>
                  </div>
                </div>
                <button type="submit" class="btn btn-success btn-lg btn-block">
                  Concluir cadastro
                </button>
              </form>
            </div>
          </div>
        </section>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>
  </body>
</html>
