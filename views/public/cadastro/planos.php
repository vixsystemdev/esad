<?php
// General settings (relative path only here)
	require ABSPATH . '/views/_includes/config.php';
?>

<!DOCTYPE html>
<html>

  <head>
    <?php require ABSPATH . '/views/_includes/metadata.php'; ?>
    <title>Planos | esad</title>
    <?php require ABSPATH . '/views/_includes/styles.php'; ?>
  </head>

  <body>
    <div class="wrapper">
      <div class="auth register">
        <header>
          <a class="brand" href="/">
            <img class="brand" src="/assets/esad/img/logo-esad.png" alt="esad">
          </a>
        </header>
        <section>
         <header class="mb-6">
            <h1 class="h2 font-weight-normal mb-3">
              Qual é o plano mais adequado para você?
            </h1>
            <p class="lead">
              Neste momento escolha qual o plano melhor se adequa às suas necessidades, <br class="hidden-sm-down">
              lembrando que todos os planos foram pensados a partir da quantidade <br class="hidden-sm-down">
              de processos que serão gerenciados pelo usuário.
            </p>
          </header>
          <?php // Pricing ?>
          <?php include "_includes/_pricing.php"; ?>
        </section>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>
  </body>

</html>