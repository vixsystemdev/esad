<?php
	// General settings (relative path only here)
	require ABSPATH . '/views/_includes/config.php';
?>

<!DOCTYPE html>
<html>
  <head>
    <?php require ABSPATH . '/views/_includes/metadata.php'; ?>
    <title>Cadastro | esad</title>
    <?php require ABSPATH . '/views/_includes/styles.php'; ?>
  </head>

  <body>
    <div class="wrapper">
      <div class="auth register">
        <header>
          <a class="brand" href="/">
            <img class="brand" src="/assets/esad/img/logo-esad.png" alt="esad">
          </a>
        </header>
        <section>
          <header class="mb-6">
            <h1 class="h2 font-weight-normal mb-3">
              Feito para advogados.
            </h1>
            <p class="lead">
              Ambiente profissional dedicado a melhorar sua gestão operacional,<br class="hidden-sm-down"> organizar e simplificar a rotina do Advogado, aumentando sua produtividade.
            </p>
          </header>
          <div class="card card-py-sm">
            <div class="card-block">
              <button type="button" class="btn btn-facebook btn-lg btn-block mt-2" onclick="location.href='concluir.php'">
                <i class="fa fa-facebook-official" aria-hidden="true"></i> Cadastrar com Facebook
              </button>
              <div class="background-line">
                <span>
                  ou
                </span>
              </div>
              <form class="form" role="form" action="concluir.php" method="post">
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="fa fa-user"></i>
                    </span>
                    <input id="name" type="text" class="form-control form-lg" name="name" placeholder="nome">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="fa fa-envelope"></i>
                    </span>
                    <input id="email" type="text" class="form-control form-lg" name="email" placeholder="email">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="fa fa-lock"></i>
                    </span>
                    <input id="password" type="password" class="form-control form-lg" name="password" placeholder="senha">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="fa fa-lock"></i>
                    </span>
                    <input id="password" type="password" class="form-control form-lg" name="password" placeholder="repetir senha">
                  </div>
                </div>
                <button type="submit" class="btn btn-success btn-lg btn-block">
                  Criar conta
                </button>
              </form>
              <div class="card-footer">
                <p class="mt-1 mb-0">
                  Ao criar uma conta, você concorda com os <br class="hidden-sm-down"><a href="#" target="_blank">Termos de Uso</a> e <a href="#" target="_blank">Política de Privacidade</a>.
                </p>
              </div>
            </div>
          </div>
        </section>
        <div class="cta">
          <a href="/login/">
            <small>
              Já é cadastrado?
            </small>
            Acesse sua conta!
          </a>
        </div>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>
  </body>

</html>