<?php
	 // General settings (relative path only here)
	 //include(HOME_URI . '/views/_includes/config.php');
	 require ABSPATH . '/views/_includes/config.php'; 
	 $page = "informativos";
?>

<!DOCTYPE html>
<html lang="pt-BR">
	<head>
		<meta charset='UTF-8'>
		<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
		<meta name='viewport' content='width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0' />
		<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>

		<title>esad | plataforma de organização advocatícia</title>

		<meta name="description" content="O esad é um sistema multiplataforma que organiza a vida profissional do advogado." />
		<meta name="keywords" content="direito,advogado,sistema,plataforma" />

		<meta name="robots" content="index,follow" />
        
		<?php 
		require ABSPATH . '/views/_includes/scripts.php';
		//include HOME_URI . "/views/_includes/styles.php"; 
		?>
	</head>

	<body class="soon">
		<div class="wrapper">
			<img src="/assets/esad/img/logo-esad.png" alt="ESAD" />
			<p>em breve</p>
		</div>
		<?php 
			require ABSPATH . '/views/_includes/scripts.php';
			//include HOME_URI . "/views/_includes/scripts.php"; 
		?>
	</body>

</html>
