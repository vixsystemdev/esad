<?php
    // General settings (relative path only here)
    require ABSPATH . '/views/_includes/config.php';

   $page = "admin";
?>

<!DOCTYPE html>
<html>
  <head>
    <?php require ABSPATH . '/views/_includes/metadata.php'; ?>
    <title>Preferências | Admin | esad</title>
    <?php require ABSPATH . '/views/_includes/styles.php'; ?>
  </head>
  <body>
    <div class="wrapper">
      <div class="layout-navbar layout-fluid <?php echo $template; ?>">
        <header>
          <?php include "_includes/navbar-fluid-admin.php"; ?>
        </header>
        <div class="wrapper-main">
          <div class="wrapper-content">
            <div class="container">
              <section>
                <header class="page-header">
                  <h1>
                    Preferências
                  </h1>
                  <p class="lead">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br class="hidden-xs-down">Eius id ratione pro ipsum dolor sit amet.
                  </p>
                </header>
                <form class="no-border" action="">
                  <div class="my-5">
                    <h2>
                      Segurança
                    </h2>
                    <p>
                      Para maior segurança, sua senha deve conter no mínimo 8 caracteres, podendo ser letras maiúsculas ou minúsculas e números.
                    </p>
                  </div>
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label for="name">
                          Senha atual *
                        </label>
                        <input name="password" id="password" type="password" class="form-control form-lg">
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label for="name">
                          Nova senha *
                        </label>
                        <input name="newPass" id="newPass" type="password" class="form-control form-lg">
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label for="name">
                          Confirmar nova senha *
                        </label>
                        <input name="newPassConfirm" id="newPassConfirm" type="password" class="form-control form-lg">
                      </div>
                    </div>
                  </div>
                  <hr>
                  <a href="#" class="btn btn-success btn-rounded btn-submit">
                    Salvar alterações
                  </a>
                </form>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>
    <?php include "_includes/modal/_share.php"; ?>
  </body>
</html>
