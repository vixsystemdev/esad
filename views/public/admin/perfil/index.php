<?php
    // General settings (relative path only here)
    require ABSPATH . '/views/_includes/config.php';

   $page = "admin";
?>

<!DOCTYPE html>
<html>
  <head>
    <?php require ABSPATH . '/views/_includes/metadata.php'; ?>
    <title>Meu Perfil | Admin | esad</title>
    <?php require ABSPATH . '/views/_includes/styles.php'; ?>
  </head>
  <body>
    <div class="wrapper">
      <div class="layout-navbar layout-fluid <?php echo $template; ?>">
        <header>
          <?php include "_includes/navbar-fluid-admin.php"; ?>
        </header>
        <div class="wrapper-main">
          <div class="wrapper-content">
            <div class="container">
              <section>
                <header class="page-header mb-8">
                  <h1>
                    Meu Perfil
                  </h1>
                </header>
                <form class="no-border" action="">
                  <div class="my-5">
                    <h2>
                      Informações Principais
                    </h2>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="name">
                          Nome completo *
                        </label>
                        <input name="name" id="name" type="text" class="form-control form-lg" value="Leonardo Fraga">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="email">
                          Email *
                        </label>
                        <input name="email" id="email" type="email" class="form-control form-lg" value="leonardofraga@gmail.com">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="phone">
                          Celular *
                        </label>
                        <input name="phone" id="phone" type="text" class="form-control form-lg" value="+55 27 99745-3073">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="telefone">
                          Telefone *
                        </label>
                        <input name="telefone" id="telefone" type="text" class="form-control form-lg" value="+55 27 3024-2357">
                      </div>
                    </div>
                  </div>
                  <hr>
                  <div class="mt-9 mb-5">
                    <h2>
                      Perfil de Acesso
                    </h2>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="access">Tipo</label>
                        <select class="form-control custom-select" id="oab-uf">
                          <option>Administrador</option>
                          <option>Operador</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <hr>
                  <a href="#" class="btn btn-success btn-rounded btn-submit">
                    <i class="fa fa-check" aria-hidden="true"></i> Salvar alterações
                  </a>
                </form>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>
  </body>
</html>
