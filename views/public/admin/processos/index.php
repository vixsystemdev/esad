<?php
   // General settings (relative path only here)
   require ABSPATH . '/views/_includes/config.php';

   $page = "admin-processos";
?>

<!DOCTYPE html>
<html>
  <head>
    <?php require ABSPATH . '/views/_includes/metadata.php'; ?>
    <title>Processos | Admin | esad</title>
    <?php require ABSPATH . '/views/_includes/styles.php'; ?>
  </head>
  <body>
    <div class="wrapper">
      <div class="layout-navbar layout-fluid <?php echo $template; ?>">
        <header>
          <?php include "_includes/navbar-fluid-admin.php"; ?>
        </header>
        <div class="wrapper-main">
          <div class="wrapper-content">
            <div class="container">
              <section>
                <header class="page-header">
                  <h1>
                    Processos
                  </h1>
                  <p class="lead">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br class="hidden-xs-down">Eius id ratione pro ipsum dolor sit amet.
                  </p>
                </header>

                <div class="card card-table">
                  <div class="card-block">
                    <div class="table-filter">
                      <button class="btn btn-default btn-block hidden-lg-up" type="button" data-toggle="collapse" data-target="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter">
                        <i class="fa fa-list" aria-hidden="true"></i> Filtrar
                      </button>
                      <form action="" class="collapse" id="collapseFilter" aria-labelledby="collapseFilter">
                        <div class="form-group">
                          <select class="form-control custom-select" id="">
                            <option value="" selected="selected" disabled="disabled" hidden>Todas matérias</option>
                            <option>Lorem ipsum</option>
                          </select>
                        </div>

                        <div class="form-group">
                          <select class="form-control custom-select" id="">
                            <option value="" selected="selected" disabled="disabled" hidden>Todas fases</option>
                            <option>Lorem ipsum</option>
                          </select>
                        </div>

                        <div class="form-group">
                          <select class="form-control custom-select" id="">
                            <option value="" selected="selected" disabled="disabled" hidden>Usuários</option>
                            <option>Lorem ipsum</option>
                          </select>
                        </div>

                        <div class="form-group">
                          <select class="form-control custom-select" id="">
                            <option value="" selected="selected" disabled="disabled" hidden>Contatos</option>
                            <option>Lorem ipsum</option>
                          </select>
                        </div>

                        <div class="form-group">
                          <select class="form-control custom-select" id="">
                            <option value="" selected="selected" disabled="disabled" hidden>Tamanho (em mb)</option>
                            <option>Lorem ipsum</option>
                          </select>
                        </div>
                      </form>
                    </div>
                    <div class="table table-responsive">
                      <table class="table table-striped table-hover">
                        <thead>
                          <tr>
                            <th>Processo</th>
                            <th class="hidden-sm-down">Matéria</th>
                            <th class="hidden-sm-down">Fase</th>
                            <th class="text-center">Usuários</th>
                            <th class="text-center">Contatos</th>
                            <th class="text-center">Tamanho (em mb)</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>
                              0203220.1002120.120102
                            </td>
                            <td class="hidden-sm-down">
                              Administrativo
                            </td>
                            <td class="hidden-sm-down">
                              Conhecimento
                            </td>
                            <td class="text-center">
                              4
                            </td>
                            <td class="text-center">
                              9
                            </td>
                            <td class="text-center">
                              102 mb
                            </td>
                          </tr>
                          <tr>
                            <td>
                              0203220.1002120.120102
                            </td>
                            <td class="hidden-sm-down">
                              Administrativo
                            </td>
                            <td class="hidden-sm-down">
                              Conhecimento
                            </td>
                            <td class="text-center">
                              4
                            </td>
                            <td class="text-center">
                              9
                            </td>
                            <td class="text-center">
                              102 mb
                            </td>
                          </tr>
                          <tr>
                            <td>
                              0203220.1002120.120102
                            </td>
                            <td class="hidden-sm-down">
                              Administrativo
                            </td>
                            <td class="hidden-sm-down">
                              Conhecimento
                            </td>
                            <td class="text-center">
                              4
                            </td>
                            <td class="text-center">
                              9
                            </td>
                            <td class="text-center">
                              102 mb
                            </td>
                          </tr>
                          <tr>
                            <td>
                              0203220.1002120.120102
                            </td>
                            <td class="hidden-sm-down">
                              Administrativo
                            </td>
                            <td class="hidden-sm-down">
                              Conhecimento
                            </td>
                            <td class="text-center">
                              4
                            </td>
                            <td class="text-center">
                              9
                            </td>
                            <td class="text-center">
                              102 mb
                            </td>
                          </tr>
                          <tr>
                            <td>
                              0203220.1002120.120102
                            </td>
                            <td class="hidden-sm-down">
                              Administrativo
                            </td>
                            <td class="hidden-sm-down">
                              Conhecimento
                            </td>
                            <td class="text-center">
                              4
                            </td>
                            <td class="text-center">
                              9
                            </td>
                            <td class="text-center">
                              102 mb
                            </td>
                          </tr>
                          
                        </tbody>
                      </table>
                    </div>
                    <nav class="pagination" aria-label="table-pagination">
                      <div class="row">
                        <div class="col-6">
                          <p class="text-muted">
                            1 - 5 de 5
                          </p>
                        </div>
                        <div class="col-6">
                          <ul class="pagination-links float-right">
                            <li class="page-item">
                              <a class="page-link" href="#">
                                <i class="fa fa-chevron-left" aria-hidden="true"></i>
                              </a>
                            </li>
                            <li class="page-item">
                              <a class="page-link active" href="#">1</a>
                            </li>
                            <?php /*
                            <li class="page-item">
                              <a class="page-link" href="#">2</a>
                            </li>
                            <li class="page-item">
                              <a class="page-link" href="#">3</a>
                            </li>
                            */ ?>
                            <li class="page-item">
                              <a class="page-link" href="#">
                                <i class="fa fa-chevron-right" aria-hidden="true"></i>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </nav>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>
  </body>
</html>
