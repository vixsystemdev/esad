<?php
	 // General settings (relative path only here)
	 require ABSPATH . '/views/_includes/config.php';

   $page = "admin";
?>

<!DOCTYPE html>
<html>
	<head>
		<?php require ABSPATH . '/views/_includes/metadata.php'; ?>
		<title>Dashboard | Admin | esad</title>
		<?php require ABSPATH . '/views/_includes/styles.php'; ?>
	</head>
	<body>
		<div class="wrapper">
			<div class="layout-navbar layout-fluid <!--?php echo $template; ?-->">
				<header>
					<?php require ABSPATH . '/views/_includes/navbar-fluid-admin.php'; ?>
				</header>
				<div class="wrapper-main">
					<div class="wrapper-content">
						<div class="container">
              <section>
  							<header class="page-header">
                  <h1>
                    Dashboard
                  </h1>
                  <p class="lead">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br class="hidden-xs-down">Eius id ratione pro ipsum dolor sit amet.
                  </p>
                </header>

                <div class="card mb-4 card-processos">
                  <div class="card-header row">
                    <h3 class="title text-left">
                      Usuários
                    </h3>
                  </div>
                  <div class="card-block">
                    <div class="heading">
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br class="hidden-xs-down">Eius id ratione pro ipsum dolor sit amet.
                      </p>
                      <h4 class="value">
                        <span class="ativos-value">
                          653
                        </span>
                        <span class="ativos-label">
                          assinantes
                        </span>
                        <span class="separator">
                          /
                        </span>
                        <span class="total-value">
                          2.130
                        </span>
                        <span class="total-label">
                          usuários
                        </span>
                      </h4>
                    </div>
                  </div>
                </div>
                <?php // Planos ?>
                <div class="row mb-4">
                  <div class="col-md-4">
                    <div class="card card-plano card-plano-1">
                      <div class="card-block">
                        <h4>
                          Plano Básico
                        </h4>
                        <p>
                          1477
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="card card-plano card-plano-2">
                      <div class="card-block">
                        <h4>
                          Plano Premium
                        </h4>
                        <p>
                          538
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="card card-plano card-plano-3">
                      <div class="card-block">
                        <h4>
                          Plano Ilimitado
                        </h4>
                        <p>
                          115
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
                <?// Recorrência ?>
                <div class="card mb-4 card-expectativa">
                  <div class="card-header row">
                    <h3 class="title text-left">
                      Receita Recorrente
                    </h3>
                  </div>
                  <div class="card-block">
                    <div class="row">
                      <div class="col-lg-6 heading">
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br class="hidden-xs-down">Eius id ratione pro ipsum dolor sit amet.
                        </p>
                        <h4 class="value">
                          R$ 15.411,00
                        </h4>
                      </div>
                      <div class="col-lg-6 data">
                        <div class="row mt-3">
                          <div class="col-md-5 col-lg-8">
                            <p class="title">
                              Plano Premium:
                            </p>
                          </div>
                          <div class="col-md-7 col-lg-4">
                            <p class="value">
                              R$ 5.111,00
                            </p>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-5 col-lg-8">
                            <p class="title">
                              Contratos Premium:
                            </p>
                          </div>
                          <div class="col-md-7 col-lg-4">
                            <p class="value">
                              R$ 4.607,50
                            </p>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-5 col-lg-8">
                            <p class="title">
                              Plano Ilimitado:
                            </p>
                          </div>
                          <div class="col-md-7 col-lg-4">
                            <p class="value">
                              R$ 5.692,50
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
						</div>
					</div>
        </div>
      </div>
    </div>
    <?php require ABSPATH . '/views/_includes/scripts.php'; ?>
  </body>
</html>
