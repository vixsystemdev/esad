<?php

class IndexController extends MainController {


    // Carrega a página
    public function index() {

        // Título da página
        $this->title = 'ESAD';

        // Parametros da função
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

        // Página de redirecionamento
        //$_SESSION['goto_url'] = HOME_URI . '/painel';
        // Modelo	
       // $modelo_geral = $this->load_model('geral-model');
		//$modelo_usuarios = $this->load_model('usuarios-model');
		//$modelo_salas = $this->load_model('salas-model');

        // Página
        require ABSPATH . '/views/public/index.php';
    }
}
