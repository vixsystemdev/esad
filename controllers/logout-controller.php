<?php
/**
 * LogoutController - Controller de logout
 *
 * Brenno Lugon
 */
class LogoutController extends MainController
{

	/**
	 * Carrega a página "/views/logout/index.php"
	 */
    public function index() {
		
		// Título da página
		$this->title = 'Logout';
				
		// Parametros da função
		$parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();
	
		$this->logout();
		
		$login_uri  = HOME_URI . '/login';
		$this->goto_page($login_uri);		
		
    } // index
} // class LogoutController