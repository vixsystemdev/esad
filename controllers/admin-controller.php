<?php

class AdminController extends MainController {

    // Carrega a página
    public function admin() {
        // Título da página
        $this->title = 'ESAD - Admin';
        // Parametros da função
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

        // Página
        //require ABSPATH . '/views/painel/includes/header.php';
        // Modelo
		//$modelo_geral      = $this->load_model('geral-model');	
		//$modelo_usuarios   = $this->load_model('usuarios-model');
        //$modelo_anuncios   = $this->load_model('anuncios-model');

        require ABSPATH . '/views/public/admin/index.php';
        //require ABSPATH . '/views/painel/includes/scripts.php';			
    }
	
	
	
}
