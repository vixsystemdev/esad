<?php

class InformativoController extends MainController {

    // Carrega a página
    public function informativos() {
        // Título da página
        $this->title = 'ESAD - Informativos';
        // Parametros da função
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

        // Página
        //require ABSPATH . '/views/painel/includes/header.php';
        // Modelo
		//$modelo_geral      = $this->load_model('geral-model');	
		//$modelo_usuarios   = $this->load_model('usuarios-model');
        //$modelo_anuncios   = $this->load_model('anuncios-model');

        require ABSPATH . '/views/public/plataforma/informativos/index.php';
        //require ABSPATH . '/views/painel/includes/scripts.php';			
    }
	
	
}
