<?php

class LoginController extends MainController {

	public function index() {
		
		// Título da página
		$this->title = 'Login';
		
		// Parametros da função
		$parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();
	
		// Verifica se o usuário está logado
		if ( $this->logged_in ) {				
		
			$login_uri  = HOME_URI . '/plataforma/meuescritorio';
			$_SESSION['goto_url'] = HOME_URI . '/plataforma/meuescritorio';
		
			// Redireciona
			echo '<meta http-equiv="Refresh" content="0; url=' . $login_uri . '">';
			echo '<script type="text/javascript">window.location.href = "' . $login_uri . '";<script>';			
			return;
		
		}
	
		/** Página de redirecionamento **/
		//$_SESSION['goto_url'] = HOME_URI . '/painel/administradorgeral/';
	
		// /views/home/login-view.php
        require ABSPATH . '/views/public/login/index.php';
			
		
    } // index


}
