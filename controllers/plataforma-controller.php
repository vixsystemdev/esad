<?php

class PlataformaController extends MainController {

    // Carrega a página
    public function meuescritorio() {
        // Título da página
        $this->title = 'ESAD - Meu Escritório';
        // Parametros da função
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

        // Página
        //require ABSPATH . '/views/painel/includes/header.php';
        // Modelo
		//$modelo_geral      = $this->load_model('geral-model');	
		//$modelo_usuarios   = $this->load_model('usuarios-model');
        //$modelo_anuncios   = $this->load_model('anuncios-model');

        require ABSPATH . '/views/public/plataforma/meu-escritorio/index.php';
        //require ABSPATH . '/views/painel/includes/scripts.php';			
    }
	
	//me ligue pra mim explicar isso ai
	
	// Carrega a página
    public function contatos() {
		
        // Título da página
        $this->title = 'Contatos | Meu Escritório';
        // Parametros da função
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();		
		
		$modelo_contatos	= $this->load_model('contatos-model');	
		
        require ABSPATH . '/views/public/plataforma/meu-escritorio/contatos/index.php';
		
    }
	
	// Carrega a página
    public function clientes() {
        // Título da página
        $this->title = 'Clientes | Meu Escritório';
        // Parametros da função
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

        // Página
        //require ABSPATH . '/views/painel/includes/header.php';
        // Modelo
		//$modelo_geral      = $this->load_model('geral-model');	
		//$modelo_usuarios   = $this->load_model('usuarios-model');
        $modelo_clientes   = $this->load_model('clientes-model');

        require ABSPATH . '/views/public/plataforma/meu-escritorio/clientes/index.php';
        //require ABSPATH . '/views/painel/includes/scripts.php';			
    }
	
	
	
	
	// Carrega a página
    public function andamentos() {
        // Título da página
        $this->title = 'Andamentos | Meu Escritório';
        // Parametros da função
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

        // Página
        
        $modelo_andamentos   = $this->load_model('andamentos-model');
        require ABSPATH . '/views/public/plataforma/meu-escritorio/andamentos/index.php';
        
    }
	
	
	// Carrega a página
    public function atividades() {
        // Título da página
        $this->title = 'Atividades | Meu Escritório';
        // Parametros da função
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

        // Página
        //require ABSPATH . '/views/painel/includes/header.php';
        // Modelo
		//$modelo_geral      = $this->load_model('geral-model');	
		//$modelo_usuarios   = $this->load_model('usuarios-model');
        $modelo_atividades   = $this->load_model('andamentos-model');

        require ABSPATH . '/views/public/plataforma/meu-escritorio/atividades/index.php';
        //require ABSPATH . '/views/painel/includes/scripts.php';			
    }
	
	
	 // Carrega a página
    public function informativos() {
        // Título da página
        $this->title = 'ESAD - Informativos';
        // Parametros da função
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

        // Página
        //require ABSPATH . '/views/painel/includes/header.php';
        // Modelo
		//$modelo_geral      = $this->load_model('geral-model');	
		//$modelo_usuarios   = $this->load_model('usuarios-model');
        //$modelo_anuncios   = $this->load_model('anuncios-model');

        require ABSPATH . '/views/public/plataforma/informativos/index.php';
        //require ABSPATH . '/views/painel/includes/scripts.php';			
    }
	
	 // Carrega a página
    public function biblioteca() {
        // Título da página
        $this->title = 'ESAD - Biblioteca';
        // Parametros da função
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

        // Página
        //require ABSPATH . '/views/painel/includes/header.php';
        // Modelo
		//$modelo_geral      = $this->load_model('geral-model');	
		//$modelo_usuarios   = $this->load_model('usuarios-model');
        //$modelo_anuncios   = $this->load_model('anuncios-model');

        require ABSPATH . '/views/public/plataforma/biblioteca/index.php';
        //require ABSPATH . '/views/painel/includes/scripts.php';			
    }
	
	public function processos() {
        // Título da página
        $this->title = 'Pro | Meu Escritório';
        // Parametros da função
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();
		
        // Página
        //require ABSPATH . '/views/painel/includes/header.php';
        // Modelo
		//$modelo_geral      = $this->load_model('geral-model');	
		//$modelo_usuarios   = $this->load_model('usuarios-model');
        $modelo_processos   = $this->load_model('processos-model');

        require ABSPATH . '/views/public/plataforma/meu-escritorio/processos/index.php';
        //require ABSPATH . '/views/painel/includes/scripts.php';			
    }
	
	
	
	public function perfil() {
        // Título da página
        $this->title = 'ESAD - Perfil';
        // Parametros da função
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

        // Página
        //require ABSPATH . '/views/painel/includes/header.php';
        // Modelo
		//$modelo_geral      = $this->load_model('geral-model');	
		//$modelo_usuarios   = $this->load_model('usuarios-model');
        //$modelo_anuncios   = $this->load_model('anuncios-model');

        require ABSPATH . '/views/public/plataforma/perfil/index.php';
        //require ABSPATH . '/views/painel/includes/scripts.php';			
    }
	
	
	public function preferencias() {
        // Título da página
        $this->title = 'ESAD - Informativos';
        // Parametros da função
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

        // Página
        //require ABSPATH . '/views/painel/includes/header.php';
        // Modelo
		//$modelo_geral      = $this->load_model('geral-model');	
		//$modelo_usuarios   = $this->load_model('usuarios-model');
        //$modelo_anuncios   = $this->load_model('anuncios-model');

        require ABSPATH . '/views/public/plataforma/preferencias/index.php';
        //require ABSPATH . '/views/painel/includes/scripts.php';			
    }
	
	public function plano() {
        // Título da página
        $this->title = 'ESAD - Plano';
        // Parametros da função
        //$parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

        // Página
        //require ABSPATH . '/views/painel/includes/header.php';
        // Modelo
		//$modelo_geral      = $this->load_model('geral-model');	
		//$modelo_usuarios   = $this->load_model('usuarios-model');
        //$modelo_anuncios   = $this->load_model('anuncios-model');

        require ABSPATH . '/views/public/plataforma/plano/index.php';
        //require ABSPATH . '/views/painel/includes/scripts.php';			
    }
	
	
	// Carrega a página
    public function arquivos() {
        // Título da página
        $this->title = 'Arquivos | Meu Escritório';
        // Parametros da função
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

        // Página
        //require ABSPATH . '/views/painel/includes/header.php';
        // Modelo
		//$modelo_geral      = $this->load_model('geral-model');	
		//$modelo_usuarios   = $this->load_model('usuarios-model');
        $modelo_arquivos   = $this->load_model('arquivos-model');

        require ABSPATH . '/views/public/plataforma/meu-escritorio/arquivos/index.php';
        //require ABSPATH . '/views/painel/includes/scripts.php';			
    }
	
	
	public function materia() {
        // Título da página
        $this->title = 'ESAD - Arquivos | Materia';
        // Parametros da função
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();
		//243
        // Página
        //require ABSPATH . '/views/painel/includes/header.php';
        // Modelo
		//$modelo_geral      = $this->load_model('geral-model');	
		//$modelo_usuarios   = $this->load_model('usuarios-model');
        //$modelo_anuncios   = $this->load_model('anuncios-model');

        require ABSPATH . '/views/public/plataforma/meu-escritorio/arquivos/materia/index.php';
        //require ABSPATH . '/views/painel/includes/scripts.php';			
    }
	
	
	public function processo() {
        // Título da página
        $this->title = 'ESAD - Arquivos | Processo';
        // Parametros da função
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();
		
        // Página
        //require ABSPATH . '/views/painel/includes/header.php';
        // Modelo
		//$modelo_geral      = $this->load_model('geral-model');	
		//$modelo_usuarios   = $this->load_model('usuarios-model');
        //$modelo_anuncios   = $this->load_model('anuncios-model');

        require ABSPATH . '/views/public/plataforma/meu-escritorio/arquivos/processo/index.php';
        //require ABSPATH . '/views/painel/includes/scripts.php';			
    }
	
	public function novoprocesso() {
        // Título da página
        $this->title = 'ESAD - Meu | Novo Processo';
        // Parametros da função
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();
		
        // Página
        //require ABSPATH . '/views/painel/includes/header.php';
        // Modelo
		//$modelo_geral      = $this->load_model('geral-model');	
		//$modelo_usuarios   = $this->load_model('usuarios-model');
        //$modelo_anuncios   = $this->load_model('anuncios-model');

        require ABSPATH . '/views/public/plataforma/meu-escritorio/novo-processo/passo-1.php';
        //require ABSPATH . '/views/painel/includes/scripts.php';			
    }
	
	public function avancaprocesso() {
        // Título da página
        $this->title = 'ESAD - Meu | Novo Processo';
        // Parametros da função
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();
		
        // Página
        //require ABSPATH . '/views/painel/includes/header.php';
        // Modelo
		//$modelo_geral      = $this->load_model('geral-model');	
		//$modelo_usuarios   = $this->load_model('usuarios-model');
        //$modelo_anuncios   = $this->load_model('anuncios-model');

        require ABSPATH . '/views/public/plataforma/meu-escritorio/novo-processo/passo-2.php';
        //require ABSPATH . '/views/painel/includes/scripts.php';			
    }
	
	
	public function processox() {
        // Título da página
        $this->title = 'ESAD - Meu Escritorio | Resumo de Processo';
        // Parametros da função
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();
		
        // Página
        
        $modelo_processo_resumo   = $this->load_model('clientes-model');

        require ABSPATH . '/views/public/plataforma/meu-escritorio/processos/processo-x/index.php';
        //require ABSPATH . '/views/painel/includes/scripts.php';			
    }
	
	public function feedback() {
        // Título da página
        $this->title = 'ESAD - Meu Escritorio | Feedback';
        // Parametros da função
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();
		
        // Página
        //require ABSPATH . '/views/painel/includes/header.php';
        // Modelo
		//$modelo_geral      = $this->load_model('geral-model');	
		//$modelo_usuarios   = $this->load_model('usuarios-model');
        $modelo_feedbacks   = $this->load_model('clientes-model');

        require ABSPATH . '/views/public/plataforma/meu-escritorio/clientes/feedback/index.php';
        //require ABSPATH . '/views/painel/includes/scripts.php';			
    }
	
	
}
