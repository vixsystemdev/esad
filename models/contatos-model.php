<?php 

class ContatosModel extends MainModel
{
 	/**
	 * Construtor para essa classe
	 *
	 * Configura o DB, o controlador, os parâmetros e dados do usuário.
	**/
	public function __construct( $db = false, $controller = null ) {
		// Configura o DB (PDO)
		$this->db = $db;
		
		// Configura o controlador
		$this->controller = $controller;
 
		// Configura os parâmetros
		$this->parametros = $this->controller->parametros;
 
		// Configura os dados do usuário
		$this->userdata = $this->controller->userdata;
		
		$this->form_data['imagem'] = 'usuario_placeholder.png';
				
	}	
	
    /**
	 * Lista contato do clientegeral
	**/
	public function listar_contatos () {
	
		
		
		// Configura as variáveis que vamos utilizar
		if ( chk_array( $this->parametros, 0 ) == 'busca') {
			
			 $id = chk_array( $this->parametros, 1 ); 
			 
			 $query = $this->db->query(
					'SELECT * 
					FROM clientes 
					WHERE status = 1 
						  and nome like "%'. $id  .'%"
						   ORDER BY nome'
					);
			
			// Retorna
			return $query->fetchAll();
			
		}
		
		
		// Configura as variáveis que vamos utilizar
		$id = $where = $query_limit = null;
		
		// Verifica se um parâmetro foi enviado para carregar uma notícia
		
		
		// Faz a consulta
		$query = $this->db->query(
			'SELECT * FROM clientes WHERE status = 1 ORDER BY nome'
		);
		
		// Retorna
		return $query->fetchAll();
	}	
	
	
	/**
	 * Lista selecionar o processos do contato geral
	**/
	public function selecionar_processos_contato () {
	
		// Configura as variáveis que vamos utilizar
		$id = chk_array( $this->parametros, 1 ); 
		
		// Verifica se um parâmetro foi enviado para carregar uma notícia
		
		
		// Faz a consulta
		$query = $this->db->query(
			'SELECT * FROM processos WHERE idcliente = ?', array($id)
		);
		
		// Retorna
		return $query->fetchAll();
	}
	
	/**
	 * Lista o processo de do cliente
	**/
	public function selecionar_processos_cliente () {
			
		$id = chk_array( $this->parametros, 1 ); 
		//consulta com parametros pela consulta localizar 
		if ( chk_array( $this->parametros, 2 ) == 'buscar' ) {
			
			$buscar = chk_array( $this->parametros, 3 ); 	
			
			// Faz a consulta
			$query = $this->db->query(
				'SELECT * 
				FROM processos 
				WHERE idcliente = '.$id .'
				       and num_processo like "%'.$buscar.'%"'
			);
			
			// Retornando para os parametro
			return $query->fetchAll();								
		}
		
		$query = $this->db->query(
				'SELECT * 
				FROM processos 
				WHERE idcliente = '.$id 
				      
		);
		
		// Retorna
		return $query->fetchAll();	
	}
	
	/**
	 * Lista selecionar processo de atividade
	 **/
	public function selecionar_processos_atividades () {
	
		// Configura as variáveis que vamos utilizar				
		$id = chk_array( $this->parametros, 1 ); 
		
		
		// Faz a consulta
		$query = $this->db->query(
			'SELECT at.atividade as "atividade"  
			FROM processos pro, materias at 
			WHERE at.id = pro.idmateria and idcliente = ?
			GROUP BY at.atividade', array($id)
		);
		
		// Retorna
		return $query->fetchAll();
	}	
	
	/**
	 * Lista selecionar os processso do expecifico cliente geral
	**/
	public function selecionar_processos () {
	
		// Configura as variáveis que vamos utilizar				
		$id = chk_array( $this->parametros, 1 ); 
		
		
		// Faz a consulta
		$query = $this->db->query(
			'SELECT num_processo
			FROM processos 
			WHERE idcliente = ?', array($id)
		);
		
		// Retorna
		return $query->fetchAll();
	}						
	
	/**
	 * Selecionar o contato do cliente do perfil geral
	**/
	public function selecionar_contato () {
	
		if ( chk_array( $this->parametros, 0 ) != 'id' && chk_array( $this->parametros, 0 ) != 'feed' ) {
			
			return;
									
		}
	
		// Configura as variáveis que vamos utilizar
		$id = chk_array( $this->parametros, 1 ); 
				
		// Faz a consulta
		$query = $this->db->query(
			'SELECT * FROM clientes WHERE id = ? ORDER BY nome', array($id)
		);
		
		$this->form_data = $query->fetch();
		
		// Retorna
		return $this->form_data;
	}	
	
	/**
	 * Insere contato
	 */
	public function insere_contato() {
	
		/* 
		Verifica se algo foi postado e se está vindo do form que tem o campo
		insere_noticia.
		*/
		if ( 'POST' != $_SERVER['REQUEST_METHOD'] || empty( $_POST['insere_contato'] ) ) {
			return;
		}
		

		/*
		Para evitar conflitos apenas inserimos valores se o parâmetro edit
		não estiver configurado.
		*/
		if ( chk_array( $this->parametros, 0 ) == 'edit' ) {
			return;
		}
		
		// Só pra garantir que não estamos atualizando nada
		if ( is_numeric( chk_array( $this->parametros, 1 ) ) ) {
			return;
		}			
			
		// Remove o campo insere_notica para não gerar problema com o PDO
		unset($_POST['insere_contato']);
			
		// Insere os dados na base de dados
		$query = $this->db->insert( 'clientes', $_POST );
		
		//$id = $this->db->last_id();
		
		// Verifica a consulta
		if ( $query ) {			
			// Retorna uma mensagem
			$_SESSION[ 'mensagem' ] = 'cadastrar_sucesso';
			
			// Redirecionamento
			header('Location:  ' . HOME_URI . '/plataforma/contatos/cadastro2/' . $this->db->last_id );			
			exit();		
		}		
		// :(
		$_SESSION[ 'mensagem' ] = 'cadastrar_erro'; 
	}
	
	
	
	
	
	
	/**
	 * Insere contato
	 */
	public function insere_tipo_contato() {
	
		
		
		
		/* 
		Verifica se algo foi postado e se está vindo do form que tem o campo
		insere_noticia.
		*/
		if ( 'POST' != $_SERVER['REQUEST_METHOD'] || empty( $_POST['insere_tipo_contato'] ) ) {
			return;
		}
		

		/*
		Para evitar conflitos apenas inserimos valores se o parâmetro edit
		não estiver configurado.
		*/
		if ( chk_array( $this->parametros, 0 ) == 'edit' ) {
			return;
		}
		
		// Só pra garantir que não estamos atualizando nada
		if ( is_numeric( chk_array( $this->parametros, 1 ) ) ) {
			return;
		}			
			
		// Remove o campo insere_notica para não gerar problema com o PDO
		unset($_POST['insere_contato']);
			
		// Insere os dados na base de dados
		$query = $this->db->insert( 'clientes', $_POST );
		
		//$id = $this->db->last_id();
		
		// Verifica a consulta
		if ( $query ) {			
			// Retorna uma mensagem
			$_SESSION[ 'mensagem' ] = 'cadastrar_sucesso';
			
			// Redirecionamento
			header('Location:  ' . HOME_URI . '/plataforma/contatos/cadastro2/' . $this->db->last_id );			
			exit();		
		}		
		// :(
		$_SESSION[ 'mensagem' ] = 'cadastrar_erro'; 
	}
	
	
	
	/**
	 * Edita usuario Adm
	 */
	public function edita_usuario_adm() {
		
		// Verifica se o primeiro parâmetro é "edit"
		if ( chk_array( $this->parametros, 0 ) != 'edit' ) {
			return;
		}
		
		// Verifica se o segundo parâmetro é um número
		if ( ! is_numeric( chk_array( $this->parametros, 1 ) ) ) {
			return;
		}
		
		// Configura o ID do produto
		$id = chk_array( $this->parametros, 1 );
			
		/* 
		Verifica se algo foi postado e se está vindo do form que tem o campo
		insere_produto.
		
		Se verdadeiro, atualiza os dados conforme a requisição.
		*/
		if ( 'POST' == $_SERVER['REQUEST_METHOD'] && ! empty( $_POST['insere_usuario'] ) ) {
		
			// Remove o campo insere_produto para não gerar problema com o PDO
			unset($_POST['insere_usuario']);			
			
			// Atualiza os dados
			$query = $this->db->update('usuarios', 'id', $id, $_POST);
			
			// Verifica a consulta
			if ( $query ) {			
				// Retorna uma mensagem
				$_SESSION[ 'mensagem' ] = 'editar_sucesso';
				
				// Redirecionamento
				header('Location:  ' . HOME_URI . '/painel/usuarios/');			
				exit();		
			}		
			// :(
			$_SESSION[ 'mensagem' ] = 'editar_erro';			
		}
		
		// Faz a consulta para obter o valor
		$query = $this->db->query(
			'SELECT * FROM usuarios WHERE id = ? LIMIT 1',
			array( $id )
		);		
		
		// Obtém os dados
		$fetch_data = $query->fetch();
		
		// Se os dados estiverem nulos, não faz nada
		if ( empty( $fetch_data ) ) {
			return;
		}		
		
		// Configura os dados do formulário
		$this->form_data = $fetch_data;		
	}
	
	
	public function edita_usuario () {
		
        // Configura o ID do produto
        $id = $this->userdata['id'];

        /*
          Verifica se algo foi postado e se está vindo do form que tem o campo
          insere_produto.

          Se verdadeiro, atualiza os dados conforme a requisição.
         */
		 
        if ('POST' == $_SERVER['REQUEST_METHOD'] && !empty($_POST['insere_perfil'])) {

            // Remove o campo insere_produto para não gerar problema com o PDO
            unset($_POST['insere_perfil']);
	
				// Tenta enviar a imagem
				$imagem = $this->upload_imagem();
				
				// Verifica se a imagem foi enviada
				if ( $imagem )
					// Adiciona a imagem no $_POST
					$_POST['imagem'] = $imagem;			


			unset($_POST['imagemcrop']);
			
            // Atualiza os dados
            $query = $this->db->update('usuarios', 'id', $id, $_POST);

            // Verifica a consulta
            if ($query) {
                // Retorna uma mensagem
                $_SESSION['mensagem'] = 'editar_sucesso';

                // Redirecionamento
                header('Location:  ' . HOME_URI . '/perfil');
                exit();
            }
            // :(
            $_SESSION['mensagem'] = 'editar_erro';
        }

        // Faz a consulta para obter o valor
        $query = $this->db->query(
                'SELECT * from usuarios WHERE id = ? LIMIT 1', array($id)
        );

        // Obtém os dados
        $fetch_data = $query->fetch();

        // Se os dados estiverem nulos, não faz nada
        if (empty($fetch_data)) {
            return;
        }

        // Configura os dados do formulário
        $this->form_data = $fetch_data;
    }
	
	public function alterar_senha () {
		
        // Configura o ID do produto
        $id = $this->userdata['id'];

        if ('POST' == $_SERVER['REQUEST_METHOD'] && !empty($_POST['alterar_senha'])) {

			$senhaatual = $_POST['senhaatual'];
			$senhabd    = $this->userdata['senha'];
			$novasenha  = $_POST['novasenha'];			
			
			unset($_POST);
			
			if ($senhaatual == $senhabd)
			{				
				$_POST['senha'] = hash('sha512', $novasenha);
								
				$query = $this->db->update('usuarios', 'id', $id, $_POST);
				 // Verifica a consulta
            	if ($query) {
                // Retorna uma mensagem
					$_SESSION['mensagem'] = 'alterar_senha_sucesso';
	
					// Redirecionamento
					header('Location:  ' . HOME_URI . '/perfil');
					exit();
            	}
				$_SESSION['mensagem'] = 'alterar_senha_erro';
			}           
            else {
				// :(
				$_SESSION['mensagem'] = 'senha_atual_incorreta';
				header('Location:  ' . HOME_URI . '/perfil');
				exit();
			}
        }

    }
	
	
	public function apaga_contato() {
		
		// O parâmetro del deverá ser enviado
		if ( chk_array( $this->parametros, 0 ) != 'del' ) {
			return;
		}
		
		// O parâmetro del deverá ser enviado
		if ( chk_array( $this->parametros, 2 ) != 'confirma' ) {
			return;
		}
		
		// O segundo parâmetro deverá ser um ID numérico
		if ( ! is_numeric( chk_array( $this->parametros, 1 ) ) ) {
			return;
		}
		
		// Configura o ID da notícia
		$id = (int)chk_array( $this->parametros, 1 );
		
		$_POST['status'] = 2;
		
		// Executa a consulta
		$query = $this->db->update( 'clientes', 'id', $id , $_POST );
		
		// Verifica a consulta
		if ( $query ) {			
			// Retorna uma mensagem
			$_SESSION[ 'mensagem' ] = 'excluir_sucesso';
			
			// Redirecionamento
			header('Location:  ' . HOME_URI . '/plataforma/contatos/');			
			exit();		
		}		
		// :(
		$_SESSION[ 'mensagem' ] = 'excluir_erro';		
	}
	
	/**
	 * Envia a imagem
	 *
	 * @since 0.1
	 * @access public
	 */	
	public function upload_imagem() {
		$imagem = $_FILES['imagem'];
			
		// Verifica se o arquivo da imagem existe
		if ( empty($imagem['name'] ) ) {
			// Remove o post da imagem
			unset($_POST['imagem']);
			// Nome da foto padrão de notícias
			//$nome_imagem = 'usuario_placeholder.png';			
			//return $nome_imagem;
		}
		
		// Configura os dados da imagem
		// Nome e extensão
		$nome_imagem    = strtolower( $imagem['name'] );
		$ext_imagem     = explode( '.', $nome_imagem );
		$nome_imagem    = $ext_imagem[0];
		$ext_imagem     = end( $ext_imagem );		
		$nome_imagem    = preg_replace( '/[^a-zA-Z0-9]/', '', $nome_imagem);
		$nome_imagem   .= '_' . date('YmdHis') . '.' . $ext_imagem;
		
		$crop    = $_POST['imagemcrop'];	
		$crop    = str_replace('data:image/png;base64,', '', $crop);
		$crop    = str_replace(' ', '+', $crop);
		$crop    = base64_decode($crop);
		
		// Tenta mover o arquivo enviado
		if ( ! file_put_contents( UP_ABSPATH . '/painel/usuarios/' . $nome_imagem, $crop) ) {
			// Retorna uma mensagem
			$this->form_msg = '<p class="error">Erro ao enviar imagem.</p>';
			return;
		}
		
		// Retorna o nome da imagem
		return $nome_imagem;		
	}
	
}